package nl.infosys.c1.hartigehap.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * 
 * This domain model stores item data. 
 * 
 * */
public class Item {

    public static BigDecimal percentage(BigDecimal base, BigDecimal pct) {
        return base.divide(ONEHUNDERT).multiply(pct);
    }

    private static final BigDecimal ONEHUNDERT = new BigDecimal(100);

    // cents after comma
    private static final int SCALE = 2;
    
    
    private final int itemID;
    
    // 1=drinks 2=food
    private final int type;

    private final BigDecimal priceEx;
    // data needs to be loaded in real-time table view so it needs properties
    private IntegerProperty superOrderID;
    private StringProperty name;

    private StringProperty description;
    // only to make JavaFX tableview working
    private ObjectProperty<BigDecimal> priceInc;
    private final IntegerProperty tax;
    private IntegerProperty count;

    private ObjectProperty<BigDecimal> totalprice;
    private Category category;

    // needed for real time tables
    private final StringProperty orderstatus;

    /**
     * Make new item
     * 
     * @param ItemID
     * @param name
     * @param superorderID only needed to make table view working
     * @param description of item
     * @param type 1=food 2=drink
     * @param price exclusive tax
     * @param tax in percents e.g. 21 (%)
     * @param Category item belongs to Category
     * 
     * */

    public Item(int itemID, String name, int superOrderID, String description,
            int type, BigDecimal price, int tax, int count, Category category) {
        this.itemID = itemID;
        this.type = type;

        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(description + "\n");
        this.count = new SimpleIntegerProperty(count);

        this.tax = new SimpleIntegerProperty(tax);

        this.superOrderID = new SimpleIntegerProperty(superOrderID);
 

        this.priceEx = price;
        this.priceInc = new SimpleObjectProperty<BigDecimal>(new BigDecimal(0.00));

        this.totalprice = new SimpleObjectProperty<BigDecimal>(new BigDecimal(0.00));
        
        final BigDecimal taxInMoney = percentage(priceEx, new BigDecimal(
                getTax()));
        BigDecimal taxedTotal = priceEx.add(taxInMoney);
        

        taxedTotal = taxedTotal.multiply(new BigDecimal(getCount()));
        totalprice.set(taxedTotal);
        

        setCategory(category);
        orderstatus = new SimpleStringProperty("");
    }

   /**
    * Copy constructor http://stackoverflow.com/a/7596965/2508481
    * 
    * @param copyItem item which needs copy
    */
    public Item(Item item) {
        this(item.getItemID(), item.getName(), item.getSuperOrderID().get(),
                item.getDescription(), item.getType(), item.getPriceEx(), item
                .getTax(), item.getCount(), item.getCategory());
    }
    
    /**
     * Adds important message to description e.g. contains alchohol, contains peanuts.
     * @param text which needs to be added
     * */
    public void addImportantText(String text) {
        description.set(description.get() + text.toLowerCase() + ", ");
    }

    /**
     * 
     * This function is called from the buttons in the table view
     * To change count and the total price (count * price(inclusive taxes) )
     * 
     * @param count - can be negative or positive e.g. 1, -1 or even 6 or -6
     * */
    public void changeCount(int howmuch) {

        final BigDecimal taxInMoney = percentage(priceEx, new BigDecimal(
                getTax()));
        BigDecimal taxedTotal = priceEx.add(taxInMoney);
        count.set(getCount() + howmuch);

        taxedTotal = taxedTotal.multiply(new BigDecimal(getCount()));
        totalprice.set(taxedTotal);
    }

    public Category getCategory() {
        return category;
    }

    public int getCount() {
        return count.get();
    }

    public IntegerProperty getCountTable() {
        return count;
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty getDescriptionTable() {
        return description;
    }

    public int getItemID() {
        return itemID;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty getNameTable() {
        return name;
    }

    public BigDecimal getPriceEx() {
        return priceEx;
    }

    public BigDecimal getPriceInc() {
        return priceInc.get();
    }

    public ObjectProperty<BigDecimal> getPriceTable() {
        return priceInc;
    }

    public StringProperty getStatusTable() {
        return orderstatus;
    }

    public IntegerProperty getSuperOrderID() {
        return superOrderID;
    }

    public int getTax() {
        return tax.get();
    }

    public IntegerProperty getTaxTable() {
        return tax;
    }

    public ObjectProperty<BigDecimal> getTotalprice() {
        return totalprice;
    }
    /**
     * @return price exclusive tax
     * */
    public BigDecimal getTotalPriceEx() {
        return priceEx.multiply(new BigDecimal(getCount()));
    }
    /**
     * @return price inclusive tax
     * */
    public BigDecimal getTotalPriceInc() {
        final BigDecimal taxInMoney = percentage(priceEx, new BigDecimal(
                getTax()));
        BigDecimal taxedTotal = priceEx.add(taxInMoney);
      
       

        taxedTotal = taxedTotal.multiply(new BigDecimal(getCount()));
        
        return taxedTotal;
    }

    /**
     * Needed for making table view working (JavaFX)
     * */
    public ObjectProperty<BigDecimal> getTotalPriceTable() {
        return totalprice;
    }

    public int getType() {
        return type;
    }

   /**
    *  Reset table values is called when order is send and when user checkout.
    *  */
    public void resetToStandard() {
        superOrderID.set(0);
        count.set(0);
        totalprice.set(new BigDecimal(0.00));
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setStatus(String status) {
        orderstatus.set(status);
    }

    public void setSuperOrderID(int id) {
        superOrderID.set(id);
    }

}
