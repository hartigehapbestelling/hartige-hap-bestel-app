package nl.infosys.c1.hartigehap.domain;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * The bill stores all the billed orders and is created on checkout. When the
 * bill is added to database the boolean isSend becomes true;
 *
 *
 **/
public class Bill {
    private int billID;
    private final List<Order> billedOrders;
    private boolean isSend;
    private final int customerID;

    /**
     * @param orders
     *            An List with all orders which needs to be paid
     * @param customerID
     *            An customer ID which belongs to this bill.
     * */
    public Bill(List<Order> billedOrders, int customerID) {

        this.customerID = customerID;
        this.billedOrders = billedOrders;
    }

    /**
     * @return List of all orders which belongs to this bill.
     * @see List
     * */
    public List<Order> getBilledOrders() {
        return billedOrders;
    }

    /**
     * @return billID
     * @see int
     * */
    public int getBillID() {
        return billID;
    }

    /**
     * @return customer ID which belongs to this bill.
     * @see int
     * */
    public int getCustomerID() {
        return customerID;
    }

    /**
     * @param boolean - Does the price needs to be included with tax?
     * @return BigDecimal - Total price of all orders in the bill.
     * @see BigDecimal
     * */
    public BigDecimal getPrice(boolean tax) {

        BigDecimal price = new BigDecimal("0");

        for (final Order order : billedOrders) {
            if (tax) {
                price = price.add(order.getTotalPriceItems(true));
            } else {
                price = price.add(order.getTotalPriceItems(false));
            }

        }
        return price;
    }

    /**
     * @return true when bill is send to database.
     * @see boolean
     * */
    public boolean isSend() {
        return isSend;
    }

    /**
     * @param bill_id
     *            - autoincrement id from database.
     * */
    public void setBillId(int billID) {
        this.billID = billID;
    }

    /**
     * @param isSend
     *            ? send=true, not send = false
     *
     * */
    public void setSend(boolean isSend) {
        this.isSend = isSend;
    }

}
