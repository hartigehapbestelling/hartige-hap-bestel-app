package nl.infosys.c1.hartigehap.domain;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import nl.infosys.c1.hartigehap.main.Main;

/*
 *
 *
 *
 *
 * */

public class Order {

    private int orderID; // this id is set by the auto increment key of the
    // database when inserting it.

    private final Timestamp sendOn;

    // 1 = restaurant - 2 = drinks bar
    private final int destination;

    // e.g. 1= placed, 2= received,3 = served
    private int statusID;

    // All items which in this order
    private List<Item> items;

    /**
     * @param destination 1=restaurant, 2=bar
     * @param orderedItems items which belong to order can only contain items of one type e.g. food or drink
     * 
     * */
    public Order(int destination, List<Item> orderedItems) {
        orderID = -1;
        this.destination = destination;
        items = new ArrayList<Item>();

        final Date today = new Date();
        sendOn = new Timestamp(today.getTime());
        statusID = 0;
        items = orderedItems;

    }

    /**
     * @return destination 1=food 2=drink
     * */
    public int getDestination() {
        return destination;
    }

    public int getItemCount() {
        return items.size();
    }

    public List<Item> getItems() {
        return items;
    }

    public int getOrderID() {
        return orderID;
    }

    public IntegerProperty getOrderIDTable() {
        return new SimpleIntegerProperty(orderID);
    }

    public Timestamp getSendOn() {
        return sendOn;
    }

    public String getStatus() {
        return Main.getOrderManager().getStatusFromId(statusID);
    }

    public int getStatusID() {
        return statusID;
    }

    public StringProperty getStatusTable() {
        return new SimpleStringProperty(Main.getOrderManager().getStatusFromId(
                statusID));
    }

    /**
     * @param boolean - Does the price needs to be included with tax?
     * @return BigDecimal - Total price of all items in the order.
     * */
    public BigDecimal getTotalPriceItems(boolean prijsinc) {
        BigDecimal totaalprijs = new BigDecimal(0);

        for (final Item item : items) {
            if (prijsinc) {
                totaalprijs = totaalprijs.add(item.getTotalPriceInc());
            } else {
                totaalprijs = totaalprijs.add(item.getTotalPriceEx());
            }
        }
        return totaalprijs;
    }

    /**
     * @return true when order is send to database
     * */
    public boolean isSend() {
        if (statusID < 1 || orderID == -1) {
            return false;
        }
        return true;
    }

    /**
     * This function set the order id upon database insert (the auto increment ID)
     * */
    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public void setStatus(int statusID) {
        this.statusID = statusID;
        for (final Item item : items) {
            item.setStatus(getStatus());
        }

    }

    public void setStatusTest(int statusID) {
        this.statusID = statusID;

    }

}