package nl.infosys.c1.hartigehap.domain;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The category domain stores a category and is created in thee CategoryDAO.
 * There are main categories and sub categories.
 *
 * For example entree (main) - soups - salads - etc
 * */

public class Category {
    private final IntegerProperty categoryID;
    private final StringProperty name;
    private final IntegerProperty consecution;
    private Category maincategory;
    private final IntegerProperty maincategoryID;

    /**
     * @param categoryID
     *            the ID of the category in the database
     * @param CategoryName
     *            the name of the category e.g. soups or salad etc.
     * @param consecution
     *            the sequence of the categories is handled with this id, comes
     *            from database
     * @param maincategoryID
     *            the ID of the category it belongs to, if not 0 it is a sub
     *            category
     * */
    public Category(int categoryID, String name, int consecution,
            int maincategoryID) {
        this.categoryID = new SimpleIntegerProperty(categoryID);
        this.name = new SimpleStringProperty(name);
        this.consecution = new SimpleIntegerProperty(consecution);
        this.maincategoryID = new SimpleIntegerProperty(maincategoryID);
    }

    /**
     * @return categoryID
     * @see int
     *
     * */
    public int getCategoryID() {
        return categoryID.get();
    }

    /**
     * @return Sequence of category
     * */
    public IntegerProperty getConsecution() {
        return consecution;
    }

    /**
     * @return MainCategory when sub category it returns the main category, else
     *         it wil return null.
     * */
    public Category getMainCategory() {
        return maincategory;
    }

    /**
     * @return returns 0 when not having main category else returns the main
     *         category id
     * @see int
     * */
    public int getMainCategoryID() {
        return maincategoryID.get();
    }

    /**
     * @return Category name
     * */
    public String getName() {
        return name.get();
    }

    /**
     * @param Category
     *            set the main category.
     * */
    public void setMainCategory(Category maincategory) {
        this.maincategory = maincategory;

    }

}
