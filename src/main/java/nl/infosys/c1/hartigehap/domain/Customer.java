package nl.infosys.c1.hartigehap.domain;

import java.sql.Date;

/**
 * 
 * This domain model keeps user data
 * */

public class Customer {
    private final int customerID;
    private final String barcode;
    private final String initials;
    private final String firstname;
    private final String lastname;
   
    //TODO: customer can't order items which are above his legal age.
    private final Date dateOfBirth;

    public Customer(int customerID, String barcode, String initials,
            String firstname, String lastname, Date dateOfBirth) {

        this.customerID = customerID;
        this.barcode = barcode;
        this.initials = initials;
        this.firstname = firstname;
        this.lastname = lastname;

        this.dateOfBirth = dateOfBirth;
    }

    public String getBarcode() {
        return barcode;
    }

    public int getCustomerID() {
        return customerID;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getInitials() {
        return initials;
    }

    public String getLastname() {
        return lastname;
    }

}
