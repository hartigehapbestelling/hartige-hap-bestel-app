package nl.infosys.c1.hartigehap.main;

/**
 * Contains most of the texts in the whole application, so we don't have to
 * search every class to change a text
 */

public final class Texts {

    public static final String TABLE_POPUP = "Tafelnummer";

    public static final String TABLE_POPUP_TEXT = "Wat is het tafelnummer?";
    public static final String WELCOME_TEXT = "Welkom, gast";

    public static final String LOGOUT_TITLE_TEXT = "Succesvol uitgelogd";

    public static final String LOGOUT_TEXT = "Iemand anders kan nu een bestelling plaatsen.";
    public static final String LOGIN_TITLE_TEXT = "Succesvol Ingelogd";

    public static final String LOGIN_TEXT = "U kunt eventueel uitloggen door uw klantenkaar opnieuw te scannen.";
    public static final String LOGIN_ERROR_TITLE_TEXT = "Niet ingelogd";

    public static final String LOGIN_ERROR_TEXT = "Barcode niet gevonden, druk op de rode hulp knop.";
    public static final String LOGIN_ERROR_ORDER_TITLE_TEXT = "Bestellingen niet aangepast";

    public static final String LOGIN_ERROR_ORDER_TEXT = "De gedane bestellingen zijn niet op uw naam ingesteld, log uit, en log nog een keer in.";
    public static final String COMPOSER_SUCCES_TITLE_TEXT = "Bestelling verzonden";

    /*
     * Order composer texts
     */

    public static final String COMPOSER_SUCCES_TEXT = "Binnenkort wordt deze geserveerd!";
    public static final String COMPOSER_ERROR_TITLE_TEXT = "Bestelling mislukt (Voorraad op)";

    public static final String COMPOSER_ERROR_TEXT = "Er is iets foutgegaan met het bestellen, spreek een medewerker aan. Waarschijnlijk"
            + "is de voorraad op.";
    /*
     * OrderOverview
     */
    public static final String STATUS_TITLE_TEXT = "Bestelling is ";

    public static final String STATIS_TEXT = "Ga naar het bestellingsoverzicht om de statussen van al uw bestellingen in te zien.";
    /*
     * BillOverview
     */
    public static final String LOGIN_CHECKOUT_TITLE_TEXT = "Er is al afgerekend";

    public static final String LOGIN_CHECKOUT_TEXT = "Je kunt nu niet meer inloggen. Druk op 'KLAAR MET BESTELLEN >'";
    public static final String BILL_SUCCES_TITLE_TEXT = "Wacht op medewerker";

    public static final String BILL_SUCCES_TEXT = "Druk nog niet rechtsonder. Er zal zo een bediende aankomen die het geld zal aannemen.";
    public static final String BILL_ERROR_TITLE_TEXT = "Oeps, er iets foutgegegaan..";

    public static final String BILL_ERROR_TEXT = "Vraag om hulp rechtsboven..";
    /*
     * 
     * Help GUI
     */
    public static final String HELP_SUCCES_TITLE_TEXT = "Bediening is gevraagd";

    public static final String HELP_SUCCES_TEXT = "De bediening is succesvol ingelicht, mocht het te lang duren dan kunt u overwegen om "
            + "naar een medewerker toe te lopen.";
    public static final String HELP_ERROR_TITLE_TEXT = "Oeps, er iets foutgegegaan..";

    public static final String HELP_ERROR_TEXT = "Er ging iets fout. Vraag daarom persoonlijk hulp aan een medewerker.";

    private Texts() {
    }

}
