package nl.infosys.c1.hartigehap.main;

import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import nl.infosys.c1.hartigehap.businesslogic.CustomerManager;
import nl.infosys.c1.hartigehap.businesslogic.ItemManager;
import nl.infosys.c1.hartigehap.businesslogic.OrderManager;
import nl.infosys.c1.hartigehap.businesslogic.TableManager;
import nl.infosys.c1.hartigehap.businesslogic.TaxManager;
import nl.infosys.c1.hartigehap.presentation.Notification;

/**
 * This is the start layer of the whole program.
 * We need to iniatilize all managers here
 * 
 * You can call the managers with e.g. Main.getCustomerManager(), Main.getItemManager() etc..
 * 
 * */
public class Main extends Application {
    public static CustomerManager getCustomerManager() {
        return customerManager;
    }

    public static ItemManager getItemManager() {
        return itemManager;
    }

    public static Notification.Notifier getNotifier() {
        return notifier;
    }

    public static OrderManager getOrderManager() {
        return orderManager;
    }

    public static TableManager getTableManager() {
        return tableManager;
    }

    public static TaxManager getTaxManager() {
        return taxManager;
    }

    /**
     * On start of application load all the managers.
     * */
    public static void main(String[] args) {
        taxManager = new TaxManager();
        itemManager = new ItemManager(false);
        customerManager = new CustomerManager();
        orderManager = new OrderManager(false);
        tableManager = new TableManager();

        launch(args);

    }

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    // BusinessLogic manager
    private static TaxManager taxManager;

    private static ItemManager itemManager;

    private static OrderManager orderManager;

    private static CustomerManager customerManager;

    private static TableManager tableManager;

    private static Notification.Notifier notifier;

    /**
     * Load the layout of the application.
     * */
    @Override
    public void start(Stage stage) {
        try {

            notifier = Notification.Notifier.INSTANCE;

            final Parent root = FXMLLoader.load(getClass().getClassLoader()
                    .getResource("OrderRootGUI.fxml"));

            final Scene scene = new Scene(root);
            //stage.setOnCloseRequest(observable -> notifier.stop());

            stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle("BestellingsApp");
            stage.setMaximized(true);
            stage.setScene(scene);

            //make sure some things reset
            stage.setOnCloseRequest(t -> {
                notifier.stop();
                
                //cancel timer
                getOrderManager().getTimer().cancel();
                

                // close timers and background threads
                Platform.exit();
                System.exit(0);
            });

            stage.show();

        } catch (final Exception e) {
            LOG.log(Level.SEVERE, e.toString(), e);
        }
    }

}
