package nl.infosys.c1.hartigehap.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This DAO caches the text values for statuses so we can link it with the id.
 * Because we don't have to join every table for the status text this is much
 * faster.
 * 
 * 
 * */
public final class StatusDAO {
    /**
     * Get statuses from database and cache it in the manager
     * 
     * @return status map with status ID and the text.
     * 
     * */
    public static Map<Integer, String> getStatusses() {

        final Map<Integer, String> statusses = new HashMap<Integer, String>();

        // First open a database connnection
        final DatabaseConnection connection1 = new DatabaseConnection();
        if (!connection1.openConnection()) {
            connection1.closeConnection();
            return null;
        }
        // If a connection was successfully setup, execute the SELECT
        // statement.
        final ResultSet resultset = connection1
                .executeSQLSelectStatement("SELECT * FROM status");

        if (resultset == null) {
            connection1.closeConnection();
            return null;
        }
        try {
            while (resultset.next()) {

                statusses.put(resultset.getInt("status_id"),
                        resultset.getString("name"));

            }
        } catch (final SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
            return null;
        }

        // We had a database connection opened. Since we're finished,
        // we need to close it.

        return statusses;

    }

    /**
     * JUnit test data
     * 
     * */
    public static Map<Integer, String> getTestStatusses() {

        final Map<Integer, String> statusses = new HashMap<Integer, String>();
        statusses.put(1, "Geplaatst");
        statusses.put(2, "In behandeling");
        statusses.put(3, "Gereed");
        statusses.put(4, "Geserveerd");
        statusses.put(5, "Betaald");

        return statusses;

    }

    private static final Logger LOG = Logger.getLogger(StatusDAO.class
            .getName());

    private StatusDAO() {
    }
}
