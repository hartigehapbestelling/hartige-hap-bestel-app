package nl.infosys.c1.hartigehap.datastorage;

import java.util.logging.Logger;

/**
 * This DAO update the status of a table in the database, so other may view it
 * and do an action.
 * 
 * For example: 1. We set table status = 1 (user want to checkout) 2. The
 * bartenders can respond because their application checks table statuses so
 * they send a bartender to the user.
 * 
 * 
 * */
public final class TableDAO {

    /**
     * The bar needs to see what table updates there are, so if a user want to
     * checkout we have to change the table status.
     * 
     * */
    public static boolean updateStatus(int tablenumber, int statusid) {
        final String sql = "UPDATE `table` SET `fk_table_status_id`="
                + statusid + " WHERE `table_number`=" + tablenumber;

        final DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()
                && connection.executeUpdateStatement(sql)) {
            return true;

        }
        connection.closeConnection();
        return false;
    }

    private TableDAO() {
    }

}
