package nl.infosys.c1.hartigehap.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class TaxDAO {

    /**
     *
     * Gets the tax percentages from database instead of joining (mySQL) the tax
     * table every time we load items.
     *
     * 
     * */
    public static Map<Integer, Integer> getTaxes() {

        final Map<Integer, Integer> taxes = new HashMap<Integer, Integer>();

        // First open a database connnection
        final DatabaseConnection connection1 = new DatabaseConnection();
        if (!connection1.openConnection()) {
            connection1.closeConnection();
            return null;
        }

        // If a connection was successfully setup, execute the SELECT
        // statement.
        final ResultSet resultset = connection1
                .executeSQLSelectStatement("SELECT * FROM tax");

        if (resultset == null) {
            connection1.closeConnection();
            return null;
        }

        try {
            while (resultset.next()) {

                taxes.put(resultset.getInt("tax_id"),
                        resultset.getInt("percents"));

            }
        } catch (final SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
            return null;
        }

        // We had a database connection opened. Since we're finished,
        // we need to close it.

        connection1.closeConnection();

        return taxes;
    }

    private static final Logger LOG = Logger.getLogger(TaxDAO.class.getName());

    private TaxDAO() {
    }

}
