package nl.infosys.c1.hartigehap.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import nl.infosys.c1.hartigehap.domain.Customer;

public final class CustomerDAO {

    /**
     * This DAO selects customer data from the database.
     * 
     * @param barcode the full input from the scanner
     * @return Customer wich belong to param barcode
     * */
    public static Customer getCustomer(String barcode) {

        Customer customer = null;

        // First open a database connnection
        final DatabaseConnection connection1 = new DatabaseConnection();
        if (!connection1.openConnection()) {
            connection1.closeConnection();
            return null;
        }

        // If a connection was successfully setup, execute the SELECT
        // statement.
        final ResultSet resultset = connection1
                .executeSQLSelectStatement("SELECT * FROM customer WHERE barcode="
                        + barcode);

        if (resultset == null) {
            connection1.closeConnection();
            return null;
        }
        try {
            if (resultset.next()) {
                customer = new Customer(resultset.getInt("customer_id"),
                        resultset.getString("barcode"),
                        resultset.getString("initials"),
                        resultset.getString("firstname"),
                        resultset.getString("lastname"),
                        resultset.getDate("date_of_birth"));

            }
        } catch (final SQLException e) {
            connection1.closeConnection();
            LOG.log(Level.SEVERE, e.toString(), e);
            return null;
        }

        // We had a database connection opened. Since we're finished,
        // we need to close it.
        connection1.closeConnection();

        return customer;
    }

    private static final Logger LOG = Logger.getLogger(CustomerDAO.class
            .getName());

    private CustomerDAO() {
    }

}
