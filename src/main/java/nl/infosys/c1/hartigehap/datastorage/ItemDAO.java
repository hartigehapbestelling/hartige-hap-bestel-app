package nl.infosys.c1.hartigehap.datastorage;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import nl.infosys.c1.hartigehap.domain.Category;
import nl.infosys.c1.hartigehap.domain.Item;

/**
 *
 * @author
 */
public final class ItemDAO {
    public static List<Item> getItems(Map<Integer, Category> categories,
            Map<Integer, Integer> taxes) {

        final List<Item> items = new ArrayList<Item>();

        // First open a database connnection
        final DatabaseConnection connection1 = new DatabaseConnection();
        if (!connection1.openConnection()) {
            connection1.closeConnection();
            return items;
        }
        // If a connection was successfully setup, execute the SELECT
        // statement.
        final ResultSet resultset = connection1
                .executeSQLSelectStatement("SELECT * FROM item");

        if (resultset == null) {
            connection1.closeConnection();
            return null;
        }
        try {
            while (resultset.next()) {

                final Item item = new Item(resultset.getInt("item_id"),
                        resultset.getString("name"), 0,
                        resultset.getString("description"),
                        resultset.getInt("fk_type_id"),
                        resultset.getBigDecimal("price"), taxes.get(resultset
                                .getInt("fk_tax_id")), 0,
                        categories.get(resultset.getInt("fk_category_id")));

                items.add(item);

            }
        } catch (final SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
            return items;
        }

        // We had a database connection opened. Since we're finished,
        // we need to close it.

        connection1.closeConnection();

        return items;
    }

    /**
     * JUnit test data
     * */
    public static List<Item> getTestItems() {

        final List<Item> items = new ArrayList<Item>();

        final Item item = new Item(1, "Cola", 0, "Lekkere cola", 2,
                new BigDecimal("1.79"), 6, 0, null);

        final Item item2 = new Item(2, "Antipasto", 0, "Heerlijke pizza", 1,
                new BigDecimal("7.36"), 6, 0, null);

        final Item item3 = new Item(3, "Bier", 0, "Lekkere bier", 2,
                new BigDecimal("1.74"), 21, 0, null);

        items.add(item);
        items.add(item2);
        items.add(item3);

        return items;
    }

    private static final Logger LOG = Logger.getLogger(ItemDAO.class.getName());

    private ItemDAO() {
    }
}