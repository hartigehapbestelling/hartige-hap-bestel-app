package nl.infosys.c1.hartigehap.datastorage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import nl.infosys.c1.hartigehap.domain.Bill;
import nl.infosys.c1.hartigehap.domain.Order;

public final class BillDAO {

    /**
     * This function inserts all bill related things in database in a
     * transaction because it's very important that all queries happen and if
     * they don't everything needs to be set
     * 
     * @throws SQLException
     *             when something went wrong
     * @return true when transaction is inserted in database.
     * */
    public static boolean addBill(Bill bill, int tablenumber)
            throws SQLException {

        // start database connection
        final DatabaseConnection connection = new DatabaseConnection();

        // check if it can connect
        if (!connection.openConnection()) {
            connection.closeConnection();
            return false;
        }

        // set customer ID
        int customerID = bill.getCustomerID();

        final String insertBillSQL = "INSERT INTO bill (ispaid,total_price,total_price_ex, fk_customer_id) VALUES (?,?,?,?)";
        final String insertBillOrderSQL = "INSERT INTO `kpt_billed_order` (`fk_bill_id`,`fk_order_id`)VALUES (?,?)";
        final String insertTableSQL = "UPDATE `table` SET `fk_table_status_id`= ? WHERE `table_number`= ?";

        PreparedStatement preparedBillStatement = null;
        PreparedStatement preparedBillOrderStatement = null;
        PreparedStatement preparedTableStatement = null;

        try {
            // START TRANSACTION
            connection.getConnection().setAutoCommit(false);

            preparedBillStatement = connection.getConnection()
                    .prepareStatement(insertBillSQL,
                            Statement.RETURN_GENERATED_KEYS);

            // not paid = 0
            preparedBillStatement.setInt(1, 0);

            // inclusive tax
            preparedBillStatement.setBigDecimal(2, bill.getPrice(true));

            // exclusive tax
            preparedBillStatement.setBigDecimal(3, bill.getPrice(false));

            // set customer ID
            if (customerID != 0) {
                preparedBillStatement.setInt(4, customerID);
            } else {
                preparedBillStatement.setString(4, null);
            }

            preparedBillStatement.executeUpdate();

            final ResultSet tableKeys = preparedBillStatement
                    .getGeneratedKeys();
            tableKeys.next();

            // get auto increment key
            final int billID = tableKeys.getInt(1);

            // Not the right value, INSERT failed
            if (billID == 0) {
                throw new SQLException();
            }

            bill.setBillId(billID);

            // all billed order insert into database
            preparedBillOrderStatement = connection.getConnection()
                    .prepareStatement(insertBillOrderSQL);

            for (final Order order : bill.getBilledOrders()) {
                preparedBillOrderStatement.setInt(1, bill.getBillID());
                preparedBillOrderStatement.setInt(2, order.getOrderID());
                preparedBillOrderStatement.addBatch();

            }

            // add to transaction
            preparedBillOrderStatement.executeBatch();

            preparedTableStatement = connection.getConnection()
                    .prepareStatement(insertTableSQL);

            preparedTableStatement.setInt(1, 2);
            preparedTableStatement.setInt(2, tablenumber);
            preparedTableStatement.executeUpdate();

            // end of transaction
            connection.getConnection().commit();

            // bill status = send.
            bill.setSend(true);

            connection.closeConnection();
            return true;

        } catch (final SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);

            // roll back database if something failed, so nothing in database is
            // wrong.
            connection.getConnection().rollback();

        } finally {
            // connection needs to close because we're done

            if (preparedBillStatement != null) {
                preparedBillStatement.close();
            }

            if (preparedBillOrderStatement != null) {
                preparedBillOrderStatement.close();
            }
            if (preparedTableStatement != null) {
                preparedTableStatement.close();
            }

        }

        connection.closeConnection();
        return false;
    }

    private static final Logger LOG = Logger.getLogger(BillDAO.class.getName());

    private BillDAO() {
    }

}
