package nl.infosys.c1.hartigehap.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.ObservableList;
import nl.infosys.c1.hartigehap.domain.Item;

public final class ImportantTextDAO {

    /**
     * This DAO selects all important texts from database and links them with items.
     * Important texts are e.g.: 18+, contains peanuts etc.
     * 
     * @return important texts from database
     * 
     * */
    public static Map<Integer, String> getImportantTexts() {
        final Map<Integer, String> importantTexts = new HashMap<Integer, String>();

        // First open a database connnection
        final DatabaseConnection connection1 = new DatabaseConnection();
        if (!connection1.openConnection()) {
            connection1.closeConnection();
            return importantTexts;
        }
        // If a connection was successfully setup, execute the SELECT
        // statement.
        final ResultSet resultset = connection1
                .executeSQLSelectStatement("SELECT * FROM importanttext");

        if (resultset == null) {
            connection1.closeConnection();
            return null;
        }
        try {
            while (resultset.next()) {

                importantTexts.put(resultset.getInt("important_id"),
                        resultset.getString("text"));

            }
        } catch (final SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
            return null;
        }

        // We had a database connection opened. Since we're finished,
        // we need to close it.
        connection1.closeConnection();
        return importantTexts;
    }

    /**
     * This load linked list between items and important texts like: 18+,
     * contains peanuts etc. After that it adds the important text to the items
     * 
     * @param items
     *            which are already cached
     * @param texts
     *            all important texts
     * */
    public static boolean loadImportantTextsKpt(ObservableList<Item> items,
            Map<Integer, String> importantTexts) {

        // First open a database connnection
        final DatabaseConnection connection1 = new DatabaseConnection();
        if (!connection1.openConnection()) {
            connection1.closeConnection();
            return false;
        }
        // If a connection was successfully setup, execute the SELECT
        // statement.
        final ResultSet resultset = connection1
                .executeSQLSelectStatement("SELECT * FROM kpt_importanttext");

        if (resultset == null) {
            connection1.closeConnection();
            return false;
        }
        try {
            while (resultset.next()) {

                final int itemid = resultset.getInt("fk_item_id");
                final int importantid = resultset.getInt("fk_important_id");

                for (final Item item : items) {
                    if (item.getItemID() == itemid) {
                        // it adds the important text to the items
                        item.addImportantText(importantTexts.get(importantid));
                    }
                }
            }
        } catch (final SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
            return false;
        }

        return true;
    }

    private static final Logger LOG = Logger.getLogger(ImportantTextDAO.class
            .getName());

    private ImportantTextDAO() {
    }

}
