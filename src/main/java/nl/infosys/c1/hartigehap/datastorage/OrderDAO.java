package nl.infosys.c1.hartigehap.datastorage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import nl.infosys.c1.hartigehap.domain.Customer;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.domain.Order;
import nl.infosys.c1.hartigehap.main.Main;

/**
 * This DAO has everything to do with orders. It inserts orders into the
 * database so other systems can keep updated with our system.
 *
 */
public final class OrderDAO {

    /**
     * @param Order
     *            object which needs to be added to database.
     * @param LoggedInCustomer
     *            which belongs to the order.
     * @param tableNumber
     *            where guest or customer is composing his order.
     * 
     * @return true when order is send to database. False when something went
     *         wrong
     * @throws SQLExeption
     * */
    public static boolean addOrder(Order order, Customer customer,
            int tablenumber) throws SQLException {

        final DatabaseConnection connection = new DatabaseConnection();

        if (!connection.openConnection()) {
            connection.closeConnection();
            return false;
        }

        int customerID = 0;
        if (customer != null) {
            customerID = customer.getCustomerID();
        }

        final String insertOrderSQL = "INSERT INTO `order` (`total_price`,`total_price_ex`,`destination`,`fk_status_id`,`fk_customer_id`,`fk_employee_id`,`send_on`) VALUES (?,?,?,?,?,?,?)";
        final String insertOrderItemSQL = "INSERT INTO `kpt_orderline` (`tax_percents`, `fk_item_id`, `fk_order_id`) VALUES (?,?,?)";
        final String insertTableSQL = "INSERT INTO `kpt_table_order` (`fk_order_id`,`fk_table_id`) VALUES (?,?)";

        PreparedStatement preparedOrderStatement = null;
        PreparedStatement preparedOrderItemStatement = null;
        PreparedStatement preparedTableStatement = null;

        try {
            // START TRANSACTION
            connection.getConnection().setAutoCommit(false);

            // return auto increment key
            preparedOrderStatement = connection.getConnection()
                    .prepareStatement(insertOrderSQL,
                            Statement.RETURN_GENERATED_KEYS);

            // inclusive taxes
            preparedOrderStatement.setBigDecimal(1,
                    order.getTotalPriceItems(true));

            // exclusive taxes
            preparedOrderStatement.setBigDecimal(2,
                    order.getTotalPriceItems(false));

            // destination of order
            // 1 = kitchen
            // 2 = bar
            preparedOrderStatement.setInt(3, order.getDestination());

            // status: 1 = send
            preparedOrderStatement.setInt(4, 1);

            // customer id
            if (customerID != 0) {
                preparedOrderStatement.setInt(5, customerID);
            } else {
                preparedOrderStatement.setString(5, null);
            }
            preparedOrderStatement.setString(6, null);

            // older MySQL versions don't work with trigger timestamp in
            // database itself.
            // so we set the currenttimestamp ourself
            preparedOrderStatement.setTimestamp(7, new java.sql.Timestamp(
                    Calendar.getInstance().getTime().getTime()));

            // add to transaction
            preparedOrderStatement.executeUpdate();

            // get auto increment key
            final ResultSet tableKeys = preparedOrderStatement
                    .getGeneratedKeys();
            tableKeys.next();
            final int orderID = tableKeys.getInt(1);

            if (orderID == 0) {
                // Not the right value, INSERT failed
                throw new SQLException();
            }

            // set the order id
            order.setOrderID(orderID);

            // all order items insert into database
            preparedOrderItemStatement = connection.getConnection()
                    .prepareStatement(insertOrderItemSQL);

            for (final Item item : order.getItems()) {
                // we don't have an amount value in database, so we need to loop
                // the count to make more queries
                for (int i = 0; i < item.getCount(); i++) {
                    preparedOrderItemStatement.setInt(1, item.getTax());
                    preparedOrderItemStatement.setInt(2, item.getItemID());
                    preparedOrderItemStatement.setInt(3, orderID);

                    preparedOrderItemStatement.addBatch();

                }

            }
            // add to transaction
            preparedOrderItemStatement.executeBatch();

            // make table working for easier working with bar
            preparedTableStatement = connection.getConnection()
                    .prepareStatement(insertTableSQL);

            preparedTableStatement.setInt(1, order.getOrderID());
            preparedTableStatement.setInt(2, tablenumber);

            // add to transaction
            preparedTableStatement.executeUpdate();

            // end of transaction
            connection.getConnection().commit();

            // order status = send.
            order.setStatus(1);

            connection.closeConnection();
            return true;

        }

        catch (final SQLException e) {
            // roll back database if something failed, so nothing in database is
            // wrong.
            connection.getConnection().rollback();
            LOG.log(Level.SEVERE, e.toString(), e);

        } finally {
            // connection needs to close because we're done

            if (preparedOrderStatement != null) {
                preparedOrderStatement.close();
            }

            if (preparedOrderItemStatement != null) {
                preparedOrderItemStatement.close();
            }
            if (preparedTableStatement != null) {
                preparedTableStatement.close();
            }

        }

        connection.closeConnection();
        return false;
    }

    /**
     * @param Orders
     *            which needs customer id reset.
     * @return return true when it succeeded.
     *
     * */

    public static boolean resetCustomer(List<Order> orders) {
        final List<String> queries = new ArrayList<String>();
        for (final Order order : orders) {
            final String sql = "UPDATE `order` SET `fk_customer_id`=NULL WHERE `order_id`="
                    + order.getOrderID();
            queries.add(sql);
        }

        final DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            return connection.executeMeerdere(queries);
        }
        connection.closeConnection();
        return false;
    }

    /**
     * @param Orders
     *            which needs customer id set.
     * @return return true when it succeeded.
     *
     * */
    public static boolean setCustomer(List<Order> orders, Customer customer) {

        final List<String> queries = new ArrayList<String>();
        for (final Order order : orders) {
            int customerID = 0;
            if (customer != null) {
                customerID = customer.getCustomerID();
            }

            final String sql = "UPDATE `order` SET `fk_customer_id`='"
                    + customerID + "' WHERE `order_id`=" + order.getOrderID();
            queries.add(sql);
        }

        final DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            return connection.executeMeerdere(queries);
        }
        connection.closeConnection();
        return false;
    }

    /**
     * @param orderList
     *            list with order which needs status update because cache needs
     *            to be updated because someone else may change it in our
     *            database and we need to keep updated.
     * */
    public static void updateOrderStatusses(List<Order> orders) {
        final DatabaseConnection connection = new DatabaseConnection();

        if (!connection.openConnection()) {
            connection.closeConnection();
            return;
        }
        for (Order order : orders) {
            final ResultSet resultset = connection
                    .executeSQLSelectStatement("SELECT `fk_status_id` FROM `order` WHERE `order_id`="
                            + order.getOrderID());

            if (resultset == null) {
                connection.closeConnection();
                return;
            }
            try {
                if (resultset.next()) {
                    final int statusID = resultset.getInt("fk_status_id");
                    if (order.getStatusID() != statusID) {
                        boolean alert = true;
                        if (statusID == 1) {
                            alert = false;
                        }
                        Main.getOrderManager().setStatus(order, false,
                                statusID, alert);
                    }

                }
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);

            }
        }

        connection.closeConnection();

    }

    /**
     * Update status from order in database
     * 
     * @return true when database update is done, false when some error occurred
     * @param Order
     *            object which needs an update
     * 
     * */
    public static boolean updateStatus(Order order) {

        final String sql = "UPDATE `order` SET `fk_status_id`="
                + order.getStatusID();

        final DatabaseConnection connection = new DatabaseConnection();
        if (connection.openConnection()) {
            return connection.executeUpdateStatement(sql);
        }
        connection.closeConnection();
        return false;

    }

    private static final Logger LOG = Logger
            .getLogger(OrderDAO.class.getName());

    private OrderDAO() {
    }

}
