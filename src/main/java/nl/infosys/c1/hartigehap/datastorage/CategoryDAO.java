package nl.infosys.c1.hartigehap.datastorage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import nl.infosys.c1.hartigehap.domain.Category;

public final class CategoryDAO {

    /**
     * This DAO selects all categories from database so we can cache it in the
     * manager
     * 
     * @return categories
     * 
     * */
    public static Map<Integer, Category> getCategories() {

        final Map<Integer, Category> categories = new HashMap<Integer, Category>();

        // First open a database connnection
        final DatabaseConnection connection1 = new DatabaseConnection();
        if (!connection1.openConnection()) {
            connection1.closeConnection();
            return null;
        }
        // If a connection was successfully setup, execute the SELECT
        // statement.
        final ResultSet resultset = connection1
                .executeSQLSelectStatement("SELECT * FROM category");

        if (resultset == null) {
            connection1.closeConnection();
            return null;
        }
        try {
            while (resultset.next()) {

                final Category category = new Category(
                        resultset.getInt("category_id"),
                        resultset.getString("name"),
                        resultset.getInt("consecution"),
                        resultset.getInt("main_category_id"));

                categories.put(resultset.getInt("category_id"), category);

            }
        } catch (final SQLException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
            connection1.closeConnection();
            return null;
        }

        // We had a database connection opened. Since we're finished,
        // we need to close it.

        connection1.closeConnection();
        return categories;
    }

    private static final Logger LOG = Logger.getLogger(CategoryDAO.class
            .getName());

    private CategoryDAO() {
    }
}
