package nl.infosys.c1.hartigehap.datastorage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @version 2.0
 *
 *          2.0: Added a bulk queries method and a method where you can get the
 *          latest auto increment id.
 */
public class DatabaseConnection {
    private static final Logger LOG = Logger.getLogger(DatabaseConnection.class
            .getName());
   /* 
    private static final String HOSTNAME = "mysql.famcoolen.nl";
    
    private static final int PORT = 3306;
    private static final String DATABASE = "avans_hartigehap_oplevering";
    private static final String USERNAME = "ivp4_admin";
    private static final String PASSWORD = "hHzCPejUBA";
    
    */
    
    private static final String HOSTNAME = "145.48.6.148";
    
    private static final int PORT = 3306;
    private static final String DATABASE = "hhc";
    private static final String USERNAME = "hhc1Admin";
    private static final String PASSWORD = "2WvXTTEWqWRLJqdT";
    
   
    

    private Connection connection;

    // The Statement object has been defined as a field because some methods
    // may return a ResultSet object. If so, the statement object may not
    // be closed as you would do when it was a local variable in the query
    // execution method.
    private Statement statement;

    public DatabaseConnection() {
        connection = null;
        statement = null;
    }

    public void closeConnection() {
        try {
            statement.close();

            // Close the connection
            connection.close();
        } catch (final Exception e) {
            LOG.log(Level.SEVERE, e.toString(), e);
        }
    }

    public boolean connectionIsOpen() {
        boolean open = false;

        if (connection != null && statement != null) {
            try {
                open = !connection.isClosed() && !statement.isClosed();
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
                open = false;
            }
        }
        // Else, at least one the connection or statement fields is null, so
        // no valid connection.

        return open;
    }

    public boolean executeInsertStatement(PreparedStatement statem) {

        boolean result = false;

        if (statem != null && connectionIsOpen()) {
            try {
                statem.execute();
                result = true;
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
                result = false;
            }
        }
        return result;
    }

    public int executeInsertStatement(String query) {

        // First, check whether a some query was passed and the connection with
        // the database.
        if (query != null && connectionIsOpen()) {
            // Then, if succeeded, execute the query.
            try {
                final Statement stmt = connection.createStatement();
                stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                final ResultSet keys = stmt.getGeneratedKeys();

                keys.next();
                return keys.getInt(1);
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
            }
        }

        return 0;
    }

    public boolean executeMeerdere(List<String> queries) {
        if (queries != null && connectionIsOpen()) {
            // Then, if succeeded, execute the query.
            try {
                final Statement s = connection.createStatement();
                for (final String query : queries) {
                    s.addBatch(query);
                }

                s.executeBatch();

                return true;
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
            }
        }

        return false;

    }

    public boolean executeSQLDeleteStatement(String query) {
        boolean result = false;

        // First, check whether a some query was passed and the connection with
        // the database.
        if (query != null && connectionIsOpen()) {
            // Then, if succeeded, execute the query.
            try {
                statement.executeUpdate(query);
                result = true;
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
                result = false;
            }
        }

        return result;
    }

    public ResultSet executeSQLSelectStatement(String query) {
        ResultSet resultset = null;

        // First, check whether a some query was passed and the connection with
        // the database.
        if (query != null && connectionIsOpen()) {
            // Then, if succeeded, execute the query.
            try {
                resultset = statement.executeQuery(query);
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
                resultset = null;
            }
        }

        return resultset;
    }

    public boolean executeUpdateStatement(String query) {

        boolean result = false;

        if (query != null && connectionIsOpen()) {
            try {
                statement.executeUpdate(query);
                result = true;
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
                result = false;
            }
        }
        return result;
    }

    public Connection getConnection() {
        return connection;
    }

    public Statement getStatement() {
        return statement;
    }

    public boolean openConnection() {
        boolean result = false;

        if (connection == null) {
            try {
                // Try to create a connection with the library database
                connection = DriverManager
                        .getConnection(
                                "jdbc:mysql://"+HOSTNAME+":"+PORT+"/" + DATABASE,
                                USERNAME, PASSWORD);

                if (connection != null) {
                    statement = connection.createStatement();
                }

                result = true;
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
                result = false;
            }
        } else {
            // A connection was already initalized.
            result = true;
        }

        return result;
    }

}
