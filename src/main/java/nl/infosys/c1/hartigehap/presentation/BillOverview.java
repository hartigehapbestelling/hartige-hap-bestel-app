package nl.infosys.c1.hartigehap.presentation;

import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Duration;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.domain.Order;
import nl.infosys.c1.hartigehap.main.Main;
import nl.infosys.c1.hartigehap.main.Texts;

public class BillOverview implements Initializable {

    private static final double DURATION = 0.1;

    @FXML
    private Button buttonLogout;

    @FXML
    private TableColumn<Item, Number> columnAantal;

    @FXML
    private TableColumn<Item, Number> columnBTW;

    @FXML
    private TableColumn<Item, String> columnNaam;

    @FXML
    private TableColumn<Item, Number> columnOrderID;

    @FXML
    private TableColumn<Item, BigDecimal> columnPrijs;

    @FXML
    private TableColumn<Item, BigDecimal> columnTotaalInc;

    // Dynamic restaurant menu
    @FXML
    private TableView<Item> menuTable;

    private OrderRoot root;

    @FXML
    private Label textLabelTotaalPrijsEx;

    // attributes
    @FXML
    private Label textLabelTotaalPrijsInc;

    /**
     * Resets bill overview for next customer.
     * */
    public void reset() {
        final ObservableList<Item> observableItemList2 = FXCollections
                .observableArrayList();

        menuTable.setItems(observableItemList2);
        menuTable.getSortOrder().add(columnOrderID);
        textLabelTotaalPrijsInc.setText("0,-");
        textLabelTotaalPrijsEx.setText("0,-");

        root.hideLoadIcon();
    }

    /*
     * Disable
     */
    public void disableButton() {
        buttonLogout.setDisable(true);
    }

    @FXML
    protected void doLogout() {

        // bill is send
        if (Main.getOrderManager().getBill().isSend()) {
            root.reset();
        } else {
            // bill is not send, do it again, else we will not checkout..

            if (Main.getOrderManager().checkout()) {
                loadBilledItems();
                buttonLogout.setText("KLAAR MET BESTELLEN →");
                Main.getNotifier().notify(
                        new Notification(Texts.BILL_SUCCES_TITLE_TEXT,
                                Texts.BILL_SUCCES_TEXT,
                                Notification.SUCCESS_ICON));
            } else {
                Main.getNotifier()
                        .notify(new Notification(Texts.BILL_ERROR_TITLE_TEXT,
                                Texts.BILL_ERROR_TEXT, Notification.ERROR_ICON));
            }

        }
        // do nothing, users get error message from database..
    }

    public void enableButton() {
        buttonLogout.setDisable(true);
        final Timeline animation = new Timeline(new KeyFrame(
                Duration.seconds(DURATION),
                actionEvent -> buttonLogout.setDisable(false)));
        animation.setCycleCount(1);
        animation.play();
    }

    public void enableReloadButton() {
        buttonLogout.setDisable(false);
        buttonLogout.setText("Opnieuw verzenden...");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        menuTable.setPlaceholder(new Label("Factuur wordt geladen..."));

        loadTable();

    }

    /**
     * Load all items from the bill, the bill contains orders and they have
     * items
     * */
    public void loadBilledItems() {
        final ObservableList<Item> observableItemList2 = FXCollections
                .observableArrayList();

        for (final Order order : Main.getOrderManager().getBill()
                .getBilledOrders()) {
            observableItemList2.addAll(order.getItems());
        }

        menuTable.setItems(observableItemList2);
        menuTable.getSortOrder().add(columnOrderID);
        setPrice();

        root.hideLoadIcon();
    }

    /**
     * Initialize table columns
     * */
    public void loadTable() {
        columnOrderID.setCellValueFactory(cellData -> cellData.getValue()
                .getSuperOrderID());
        columnOrderID.setSortType(TableColumn.SortType.ASCENDING);
        columnNaam.setCellValueFactory(cellData -> cellData.getValue()
                .getNameTable());
        columnPrijs.setCellValueFactory(cellData -> cellData.getValue()
                .getPriceTable());
        columnAantal.setCellValueFactory(cellData -> cellData.getValue()
                .getCountTable());
        columnBTW.setCellValueFactory(cellData -> cellData.getValue()
                .getTaxTable());
        columnTotaalInc.setCellValueFactory(cellData -> cellData.getValue()
                .getTotalPriceTable());

    }

    /**
     * This function sets the price in order overview, the total of all items
     * which are ordered
     * */
    private void setPrice() {
        final BigDecimal priceInc = Main.getOrderManager().getBill()
                .getPrice(true);
        final BigDecimal priceEx = Main.getOrderManager().getBill()
                .getPrice(false);

        final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');

        final DecimalFormat formatter = new DecimalFormat("0.00", symbols);

        textLabelTotaalPrijsInc.setText(formatter.format(priceInc));
        textLabelTotaalPrijsEx.setText(formatter.format(priceEx));

    }

    /**
     * Set the root controller, so we can change the root layout.
     * */
    public void setRootController(OrderRoot root) {
        this.root = root;
    }

}
