package nl.infosys.c1.hartigehap.presentation;

import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Duration;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.domain.Order;
import nl.infosys.c1.hartigehap.main.Main;
import nl.infosys.c1.hartigehap.main.Texts;

public class OrderOverview implements Initializable {

    private static final double DURATION = 0.1;

    @FXML
    private Button buttonSendBill;
    @FXML
    private TableColumn<Item, Number> columnAantal;

    @FXML
    private TableColumn<Item, String> columnNaam;
    @FXML
    private TableColumn<Item, Number> columnOrderID;

    @FXML
    private TableColumn<Item, BigDecimal> columnPrijs;

    @FXML
    private TableColumn<Item, String> columnStatus;

    @FXML
    private TableColumn<Item, BigDecimal> columnTotaal;

    @FXML
    private TableView<Item> menuTable;

    private OrderRoot root;

    // attributes
    @FXML
    private Label textLabelTotaalPrijs;

    public void reset() {
        final ObservableList<Item> observableItemList = FXCollections
                .observableArrayList();
        menuTable.setItems(observableItemList);
        menuTable.getSortOrder().add(columnOrderID);
        textLabelTotaalPrijs.setText("0,-");
        root.hideLoadIcon();
    }

    public void disableButton() {
        buttonSendBill.setDisable(true);
    }

    @FXML
    protected void doMoreOrders() {
        root.goToTabOne();
    }

    @FXML
    protected void doSendBill() {

        //go to bill overview
        root.goToTabThree();
        
        // Send order to database via backgroiund thread, no lag in application
        new Thread(() -> {

            if (Main.getOrderManager().checkout()) {
                succes();
            } else {
                error();
            }

        }).start();

    }

    /**
     * Enable checkout button
     * */
    public void enableButton() {
        buttonSendBill.setDisable(true);
        final Timeline animation = new Timeline(new KeyFrame(
                Duration.seconds(DURATION),
                actionEvent -> buttonSendBill.setDisable(false)));
        animation.setCycleCount(1);
        animation.play();

    }

    /**
     * Initialize the layout of the order overview
     * 
     * @param URL
     *            of the FXML file of the layout
     * @param resources
     *            map of all images and other files in the fxml file
     * */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        menuTable.setPlaceholder(new Label("Bestellingen worden geladen..."));

        loadTable();

    }

    /**
     * 
     * Load all ordered items in table (the items are from the order manager)
     * */
    public void loadOrders() {
        final ObservableList<Item> observableItemList = FXCollections
                .observableArrayList();

        for (final Order order : Main.getOrderManager().getSendedOrders()) {
            observableItemList.addAll(order.getItems());

        }

        menuTable.setItems(observableItemList);
        menuTable.getSortOrder().add(columnOrderID);
        setPrice();
        root.hideLoadIcon();
    }

    /**
     * Initialize table columns
     * */
    public void loadTable() {
        columnOrderID.setCellValueFactory(cellData -> cellData.getValue()
                .getSuperOrderID());
        columnOrderID.setSortType(TableColumn.SortType.DESCENDING);
        columnNaam.setCellValueFactory(cellData -> cellData.getValue()
                .getNameTable());

        columnPrijs.setCellValueFactory(cellData -> cellData.getValue()
                .getPriceTable());
        columnAantal.setCellValueFactory(cellData -> cellData.getValue()
                .getCountTable());
        columnTotaal.setCellValueFactory(cellData -> cellData.getValue()
                .getTotalPriceTable());

        columnStatus.setCellValueFactory(cellData -> cellData.getValue()
                .getStatusTable());

        columnPrijs.setStyle("-fx-alignment: CENTER-RIGHT;");
        columnAantal.setStyle("-fx-alignment: CENTER;");
        columnTotaal.setStyle("-fx-alignment: CENTER-RIGHT;");
    }

    /**
     * This function sets the price in order overview, the total of all items
     * which are ordered
     * */
    public void setPrice() {
        BigDecimal price = new BigDecimal("0.00");
        for (final Order order : Main.getOrderManager().getSendedOrders()) {
            price = price.add(order.getTotalPriceItems(true));
        }

        final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');

        final DecimalFormat formatter = new DecimalFormat("0.00", symbols);

        textLabelTotaalPrijs.setText(formatter.format(price));

    }

    /**
     * Set the root controller, so we can change the root layout.
     * */
    public void setRootController(OrderRoot root) {
        this.root = root;
    }

    /**
     * Bill is send, so we can load the billed items in the bill overview.
     * 
     * */
    public void succes() {
        // We're inside a thread so we need to get back to the application
        // thread to load the items
        Platform.runLater(() -> {

            root.getBillOverview().loadBilledItems();
            root.getBillOverview().enableButton();

            Main.getNotifier().notify(
                    new Notification(Texts.BILL_SUCCES_TITLE_TEXT,
                            Texts.BILL_SUCCES_TEXT, Notification.SUCCESS_ICON));
        });
    }

    /**
     * Bill is not send, so we notify the user and make reload button working,
     * to send bill again
     * 
     * */
    public void error() {
        Platform.runLater(() -> {
            root.getBillOverview().enableReloadButton();
            Main.getNotifier().notify(
                    new Notification(Texts.BILL_ERROR_TITLE_TEXT,
                            Texts.BILL_ERROR_TEXT, Notification.ERROR_ICON));
        });
    }

}
