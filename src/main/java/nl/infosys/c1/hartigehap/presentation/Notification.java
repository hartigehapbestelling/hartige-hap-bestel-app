/*
 * Copyright (c) 2013 by Gerrit Grunwald
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nl.infosys.c1.hartigehap.presentation;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * Created by User: hansolo Date: 01.07.13 Time: 07:10 Edited by Richard
 * Lindhout to match java conventions 18 june 2015 19:34
 */
public class Notification {

    // ******************** Inner Classes *************************************
    public enum Notifier {
        INSTANCE;

        private static double height = 140;

        private static final double ICON_HEIGHT = 24;

        private static final double ICON_WIDTH = 24;

        private static double offsetX = 0;

        private static double offsetY = 45;

        private static Pos popupLocation = Pos.TOP_CENTER;

        private static double spacingY = 5;

        private static Stage stageRef = null;
        private static double width = 900;

        /**
         * @param height
         *            The default is 80 px.
         */
        public static void setHeight(final double height) {
            Notifier.height = height;
        }

        /**
         * Sets the Notification's owner stage so that when the owner stage is
         * closed Notifications will be shut down as well.<br>
         * This is only needed if <code>setPopupLocation</code> is called
         * <u>without</u> a stage reference.
         *
         * @param OWNER
         */
        public static void setNotificationOwner(final Stage owner) {
            INSTANCE.stage.initOwner(owner);
        }

        /**
         * @param offsetX
         *            The horizontal shift required. <br>
         *            The default is 0 px.
         */
        public static void setOffsetX(final double offsetX) {
            Notifier.offsetX = offsetX;
        }

        /**
         * @param offsetY
         *            The vertical shift required. <br>
         *            The default is 25 px.
         */
        public static void setOffsetY(final double offsetY) {
            Notifier.offsetY = offsetY;
        }

        // ******************** Methods
        // *******************************************
        /**
         * @param stageRef
         *            The Notification will be positioned relative to the given
         *            Stage.<br>
         *            If null then the Notification will be positioned relative
         *            to the primary Screen.
         * @param popupLocation
         *            The default is TOP_RIGHT of primary Screen.
         */
        public static void setPopupLocation(final Stage stageRef,
                final Pos popupLocation) {
            if (null != stageRef) {
                INSTANCE.stage.initOwner(stageRef);
                Notifier.stageRef = stageRef;
            }
            Notifier.popupLocation = popupLocation;
        }

        /**
         * @param spacingy
         *            The spacing between multiple Notifications. <br>
         *            The default is 5 px.
         */
        public static void setSpacingY(final double spacingy) {
            Notifier.spacingY = spacingy;
        }

        /**
         * @param width
         *            The default is 300 px.
         */
        public static void setWidth(final double width) {
            Notifier.width = width;
        }

        private Duration popupLifetime;

        private ObservableList<Popup> popups;

        private Scene scene;

        private Stage stage;

        // ******************** Constructor
        // ***************************************
        private Notifier() {
            init();
            initGraphics();
        }

        private double calcX(final double left, final double totalwidth) {
            switch (popupLocation) {
            case TOP_LEFT:
            case CENTER_LEFT:
            case BOTTOM_LEFT:
                return left + offsetX;
            case TOP_CENTER:
            case CENTER:
            case BOTTOM_CENTER:
                return (left + ((totalwidth - width) * 0.5)) - offsetX;
            case TOP_RIGHT:
            case CENTER_RIGHT:
            case BOTTOM_RIGHT:
                return (left + totalwidth) - width - offsetX;
            default:
                return 0.0;
            }
        }

        private double calcY(final double top, final double totalHeight) {
            switch (popupLocation) {
            case TOP_LEFT:
            case TOP_CENTER:
            case TOP_RIGHT:
                return top + offsetY;
            case CENTER_LEFT:
            case CENTER:
            case CENTER_RIGHT:
                return (top + ((totalHeight - height) / 2)) - offsetY;
            case BOTTOM_LEFT:
            case BOTTOM_CENTER:
            case BOTTOM_RIGHT:
                return (top + totalHeight) - height - offsetY;
            default:
                return 0.0;
            }
        }

        /**
         * Makes sure that the given value is within the range of min to max
         *
         * @param min
         * @param max
         * @param value
         * @return
         */
        private double clamp(final double min, final double max,
                final double value) {
            if (value < min) {
                return min;
            }
            if (value > max) {
                return max;
            }
            return value;
        }

        /**
         * Returns the Duration that the notification will stay on screen before
         * it will fade out.
         *
         * @return the Duration the popup notification will stay on screen
         */
        public Duration getPopupLifetime() {
            return popupLifetime;
        }

        private double getX() {
            if (null == stageRef) {
                return calcX(0.0, Screen.getPrimary().getBounds().getWidth());
            }

            return calcX(stageRef.getX(), stageRef.getWidth());
        }

        private double getY() {
            if (null == stageRef) {
                return calcY(0.0, Screen.getPrimary().getBounds().getHeight());
            }

            return calcY(stageRef.getY(), stageRef.getHeight());
        }

        // ******************** Initialization
        // ************************************
        private void init() {
            popupLifetime = Duration.millis(3000);
            popups = FXCollections.observableArrayList();
        }

        private void initGraphics() {
            scene = new Scene(new Region());
            scene.setFill(null);
            scene.getStylesheets().add(
                    getClass().getClassLoader()
                            .getResource("css/application.css")
                            .toExternalForm());

            stage = new Stage();
            stage.initStyle(StageStyle.TRANSPARENT);
            stage.setScene(scene);
        }

        /**
         * Show the given Notification on the screen
         *
         * @param notification
         */
        public void notify(final Notification notification) {
            preOrder();
            showPopup(notification);
        }

        /**
         * Show a Notification with the given parameters on the screen
         *
         * @param title
         * @param message
         * @param image
         */
        public void notify(final String title, final String message,
                final Image image) {
            notify(new Notification(title, message, image));
        }

        /**
         * Show a Notification with the given title and message and an Error
         * icon
         *
         * @param title
         * @param message
         */
        public void notifyError(final String title, final String message) {
            notify(new Notification(title, message, Notification.ERROR_ICON));
        }

        /**
         * Show a Notification with the given title and message and an Info icon
         *
         * @param title
         * @param message
         */
        public void notifyInfo(final String title, final String message) {
            notify(new Notification(title, message, Notification.INFO_ICON));
        }

        /**
         * Show a Notification with the given title and message and a Checkmark
         * icon
         *
         * @param title
         * @param message
         */
        public void notifySuccess(final String title, final String message) {
            notify(new Notification(title, message, Notification.SUCCESS_ICON));
        }

        /**
         * Show a Notification with the given title and message and a Warning
         * icon
         *
         * @param title
         * @param message
         */
        public void notifyWarning(final String title, final String message) {
            notify(new Notification(title, message, Notification.WARNING_ICON));
        }

        /**
         * Reorder the popup Notifications on screen so that the latest
         * Notification will stay on top
         */
        private void preOrder() {
            if (popups.isEmpty()) {
                return;
            }
            for (int i = 0; i < popups.size(); i++) {
                switch (popupLocation) {
                case TOP_LEFT:
                case TOP_CENTER:
                case TOP_RIGHT:
                    popups.get(i)
                            .setY(popups.get(i).getY() + height + spacingY);
                    break;
                default:
                    popups.get(i)
                            .setY(popups.get(i).getY() - height - spacingY);
                }
            }
        }

        /**
         * Defines the Duration that the popup notification will stay on screen
         * before it will fade out. The parameter is limited to values between 2
         * and 20 seconds.
         *
         * @param popuplifetime
         */
        public void setPopupLifetime(final Duration popuplifetime) {
            popupLifetime = Duration.millis(clamp(2000, 20000,
                    popuplifetime.toMillis()));
        }

        /**
         * Creates and shows a popup with the data from the given Notification
         * object
         *
         * @param notication
         */
        private void showPopup(final Notification notication) {
            final Label titled = new Label(notication.title);
            titled.getStyleClass().add("title");

            final ImageView icon = new ImageView(notication.image);
            icon.setFitWidth(ICON_WIDTH);
            icon.setFitHeight(ICON_HEIGHT);

            final Label messaged = new Label(notication.message, icon);
            messaged.getStyleClass().add("message");

            final VBox popupLayout = new VBox();
            popupLayout.setSpacing(10);
            popupLayout.setPadding(new Insets(10, 10, 10, 10));
            popupLayout.getChildren().addAll(titled, messaged);

            final StackPane popupContent = new StackPane();
            popupContent.setPrefSize(width, height);
            popupContent.getStyleClass().add("notification");
            popupContent.getChildren().addAll(popupLayout);

            final Popup POPUP = new Popup();
            POPUP.setX(getX());
            POPUP.setY(getY());
            POPUP.getContent().add(popupContent);

            popups.add(POPUP);

            // Add a timeline for popup fade out
            final KeyValue fadeOutBegin = new KeyValue(POPUP.opacityProperty(),
                    1.0);
            final KeyValue fadeOutEnd = new KeyValue(POPUP.opacityProperty(),
                    0.0);

            final KeyFrame kfBegin = new KeyFrame(Duration.ZERO, fadeOutBegin);
            final KeyFrame kfEnd = new KeyFrame(Duration.millis(500),
                    fadeOutEnd);

            final Timeline timeline = new Timeline(kfBegin, kfEnd);
            timeline.setDelay(popupLifetime);
            timeline.setOnFinished(actionEvent -> Platform.runLater(() -> {
                POPUP.hide();
                popups.remove(POPUP);

            }));

            if (stage.isShowing()) {
                stage.toFront();
            } else {
                stage.show();
            }

            POPUP.show(stage);
            timeline.play();
        }

        public void stop() {
            popups.clear();
            stage.close();
        }
    }

    public static final Image ERROR_ICON = new Image(
            Notifier.class.getResourceAsStream("/notification/error.png"));
    public static final Image INFO_ICON = new Image(
            Notifier.class.getResourceAsStream("/notification/info.png"));
    public static final Image SUCCESS_ICON = new Image(
            Notifier.class.getResourceAsStream("/notification/success.png"));
    public static final Image WARNING_ICON = new Image(
            Notifier.class.getResourceAsStream("/notification/warning.png"));
    private final Image image;
    private final String message;

    private final String title;

    public Notification(final String message, final Image image) {
        this("", message, image);
    }

    // ******************** Constructors **************************************
    public Notification(final String title, final String message) {
        this(title, message, null);
    }

    public Notification(final String title, final String message,
            final Image image) {
        this.title = title;
        this.message = message;
        this.image = image;
    }
}