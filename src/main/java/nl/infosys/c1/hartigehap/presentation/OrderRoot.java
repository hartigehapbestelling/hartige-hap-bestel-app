package nl.infosys.c1.hartigehap.presentation;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;

import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import nl.infosys.c1.hartigehap.main.Main;
import nl.infosys.c1.hartigehap.main.Texts;

public class OrderRoot implements  Initializable{

    private static final int BARCODE_INPUT_MAX_TIME = 300;
    private static final Logger LOG = Logger.getLogger(OrderRoot.class
            .getName());
    private static final String VALID_INPUT = "[0-9]+";

    @FXML
    private BillOverview billoverviewController;
    @FXML
    private Button buttonCleartextFieldZoek;

    @FXML
    private Button buttonKlant;

    @FXML
    private Button buttonTitle;

    // cache in GUI
    private String customerbarcode;
    // needed reference to close
    private Stage helpstage;
    @FXML
    private ImageView iconLoad;

    @FXML
    private ImageView iconZoek;

    // attributes
    @FXML
    private BorderPane keyListener;
    private long lastTime;
    // tabcontrollers
    @FXML
    private OrderComposer ordercomposerController;
    @FXML
    private OrderOverview orderoverviewController;

    @FXML
    private AnchorPane searchContainer;

    @FXML
    private Tab tabAllOrders;

    @FXML
    private Tab tabCheckout;

    @FXML
    private Tab tabChooseItems;
    @FXML
    private TabPane tabPane;

    @FXML
    private TextField textFieldZoek;

    public void closeHelp() {
        helpstage.close();
    }

    @FXML
    protected void doClearTextFieldZoek() {
        textFieldZoek.clear();

        // The menu shows results from last search
        // Show them all results.
        ordercomposerController.filterFirstFood();
    }

    /**
     *
     * Button is clicked to open the help GUI, so we make a new screen with the
     * help GUI.
     *
     * */

    @FXML
    protected void doHelpOpen() {

        Parent root;
        helpstage = new Stage();
        try {
            root = FXMLLoader.load(getClass().getResource("/HelpGUI.fxml"));
            helpstage.setScene(new Scene(root));
            helpstage.setTitle("Veelgestelde vragen");
            helpstage.setMaximized(true);
            helpstage.initStyle(StageStyle.UNDECORATED);
            helpstage.initModality(Modality.APPLICATION_MODAL);
            helpstage.show();

        } catch (final IOException e) {
            LOG.log(Level.SEVERE, e.toString(), e);
        }

    }

    /**
     *
     * Scanner works like this: Same event as normal keyboard, the scanner types
     * characters from the barcode ONE by ONE within 300ms, so we need to count
     * them only within that period.
     *
     *
     * This void waits for input and checks if if 1st number is inserted, then
     * its set the lastTime to currentTime and check if the other characters are
     * within a 300ms period. If not it resets the customerBarcode.
     *
     * If number input is within 300ms, we make the customerbarcode string
     * longer with the characters till there is a ENTER Event probably the
     * scanner input!
     *
     * @param keyevent
     *            from keyboard input (in this case probably the barcode
     *            scanner)
     *
     * */
    protected void doLoginCustomer(KeyEvent t) {

        // first time?
        if (customerbarcode.length() == 1) {
            lastTime = System.currentTimeMillis();
        }

        // not within timespan
        if ((System.currentTimeMillis() - lastTime) > BARCODE_INPUT_MAX_TIME) {
            resetBarcode();
        }

        // if text AND not an enter RESETBARCODE
        if (!t.getText().matches(VALID_INPUT) && (t.getCode() != KeyCode.ENTER)) {
            resetBarcode();
            return;
        }

        // Still within timespan, check if there's an ENTER (end of barcode)
        if (t.getCode() == KeyCode.ENTER) {

            if (customerbarcode == "") {
                return;
            }
            textFieldZoek.clear();

            // is on checkout so user can't logout via barcode scanner anymore
            // but needs to click on a button to logout.
            if (Main.getOrderManager().getBill() != null) {
                Main.getNotifier().notify(
                        new Notification(Texts.LOGIN_CHECKOUT_TITLE_TEXT,
                                Texts.LOGIN_CHECKOUT_TEXT,
                                Notification.WARNING_ICON));

                return;
            }

            // already logged in, so log out
            if (Main.getCustomerManager().getLoggedInCustomer() != null) {

                // customer wants to go back to guest mode

                // If not has checked out
                // reset all customer id from orders in database
                if (Main.getOrderManager().hasOrdered()) {
                    Main.getCustomerManager().reset(true);
                } else {
                    Main.getCustomerManager().reset(false);
                }

                buttonKlant.setText(Texts.WELCOME_TEXT);
                resetBarcode();
                return;
            }

            // remove whitespace
            customerbarcode = customerbarcode.replaceAll("\\s", "");

            // login from database
            if (Main.getCustomerManager().login(customerbarcode, true) != null) {
                // user data is fetched from database

                // set user welcome text in application GUI
                buttonKlant.setText(Main.getCustomerManager()
                        .getLoggedInCustomer().getInitials()
                        + " "
                        + Main.getCustomerManager().getLoggedInCustomer()
                                .getLastname());

                // notify customer
                Main.getNotifier().notify(
                        new Notification(Texts.LOGIN_TITLE_TEXT, "Welkom, "
                                + Main.getCustomerManager()
                                        .getLoggedInCustomer().getFirstname()
                                + " "
                                + Main.getCustomerManager()
                                        .getLoggedInCustomer().getLastname()
                                + " " + Texts.LOGIN_TEXT,
                                Notification.SUCCESS_ICON));

            } else {
                // show login error
                Main.getNotifier()
                        .notify(new Notification(Texts.LOGIN_ERROR_TITLE_TEXT,
                                Texts.LOGIN_ERROR_TEXT, Notification.ERROR_ICON));

            }
            // whether user login was successful or not, we reset barcode for
            // next time application has barcode input
            resetBarcode();

        }

        // within time period (300ms) and no enter events, we count the numbers
        // of the
        // barcode
        customerbarcode += t.getText();

    }

    /**
     * Key event on search menu
     * */
    @FXML
    protected void doSearchMenu() {
        final String value = textFieldZoek.getText();
        doSearchMenu(value);
    }

    /**
     * Search items for
     * 
     * @param searchtext
     *            e.g. Coke or Beer
     * */
    public void doSearchMenu(String value) {
        // if it contains numbers it is barcode input, only search for text
        if (value.matches(VALID_INPUT)) {
            return;
        }
        if (ordercomposerController.doSearch(value)) {
            ordercomposerController.setTransparentButtons();
            buttonCleartextFieldZoek.setVisible(true);
            iconZoek.setVisible(false);
        } else {
            ordercomposerController.setTransparentButtons();
            ordercomposerController.filterFirstFood();
            buttonCleartextFieldZoek.setVisible(false);
            iconZoek.setVisible(true);
        }

    }

    public BillOverview getBillOverview() {
        return billoverviewController;
    }

    public OrderOverview getOrderOverview() {
        return orderoverviewController;
    }

    /**
     * Go to order composer
     * */
    public void goToTabOne() {
        setTitleText("", false);
        hideLoadIcon();

        ordercomposerController.filterFirstFood();
        tabPane.getSelectionModel().select(tabChooseItems);
    }

    /**
     * Go to send orders overview
     * */
    public void goToTabThree() {
        showLoadIcon();
        tabPane.getSelectionModel().select(tabCheckout);
        billoverviewController.disableButton();
        setTitleText("Factuur", true);

    }

    /**
     * Go to bill overview
     * */
    public void goToTabTwo() {
        showLoadIcon();
        tabPane.getSelectionModel().select(tabAllOrders);

        setTitleText("Bestellingen", true);

        orderoverviewController.disableButton();

    }

    /**
     * Hide the loading indicator.
     * */
    public void hideLoadIcon() {
        iconLoad.setVisible(false);
    }

    /**
     * Initialize the root of the application layout
     * 
     * @param URL
     *            of the FXML file of the layout
     * @param resources
     *            map of all images and other files in the fxml file
     * */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        resetBarcode();

        // show popup for employee to enter table number
        showTableNumberPopup();

        // initialize embedded controllers to communicate later on.
        ordercomposerController.setRootController(this);
        orderoverviewController.setRootController(this);
        billoverviewController.setRootController(this);

        // Key listener for customer login with bar code scanner
        keyListener.setOnKeyReleased(t -> {
            // close program when someone attached a keyboard and press
            // escape (only employees have keyboard access)
                if (t.getCode() == KeyCode.ESCAPE) {
                    Platform.exit();
                }
                // If the user focus on search field and scans his customer pas
                // do not search but clear the field..
                if ((t.getSource() == textFieldZoek)
                        && !t.getText().matches(VALID_INPUT)) {
                    return;
                }

                doLoginCustomer(t);
            });

    }

    @FXML
    protected void onFocusRoot() {
        // We decided that the user needs to close the
        // keyboard via the close button, but maybe in the future we will do
        // this automatically when user clicks something outside an input or
        // when JavaFX supports tablets
    }

    /**
     * When user clicks or touch the text field
     * */
    @FXML
    protected void onFocusTextFieldZoek() {
        // we're going to need a keyboard popup
        showVirtualKeyboard();
    }

    /**
     * Customer did checkout Empty cache which is user related.
     * */
    public void reset() {

        // reset status to empty
        Main.getTableManager().reset();

        // empty cache with user but not from database.
        Main.getCustomerManager().reset(false);

        // empty order cache
        Main.getOrderManager().reset();

        // clear items count
        Main.getItemManager().reset();

        // empty order overview
        orderoverviewController.reset();

        // empty bill overview
        billoverviewController.reset();

        // reset user related text in program
        buttonKlant.setText(Texts.WELCOME_TEXT);
        goToTabOne();

        // show logout message
        Main.getNotifier().notify(
                new Notification(Texts.LOGOUT_TITLE_TEXT, Texts.LOGOUT_TEXT,
                        Notification.SUCCESS_ICON));

    }

    /**
     * Resets barcode because it's not within 300ms timespan.
     * */
    public void resetBarcode() {
        customerbarcode = "";
    }

    private void setTitleText(String text, boolean visible) {
        if (visible) {
            searchContainer.setVisible(false);
            buttonTitle.setVisible(true);
            buttonTitle.setText(text);
        } else {
            searchContainer.setVisible(true);
            buttonTitle.setVisible(false);
            buttonTitle.setText("");
        }
    }

    public void showLoadIcon() {
        iconLoad.setVisible(true);
    }

    /**
     * Show a text popup where employee can enter the table number on initialize of
     * the application
     * 
     * */
    private void showTableNumberPopup() {
        
        //Jenkins on Avans server does not support Java 8_0_45
        
        // Do remove the /* and the */ on production
        
       /* TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle(Texts.TABLE_POPUP);
        dialog.setHeaderText(Texts.TABLE_POPUP_TEXT);
        dialog.setContentText(Texts.TABLE_POPUP);

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() && result.get().matches(VALID_INPUT)) {

            int tablenumber = Integer.parseInt(result.get());

            // sets the table number in the manager
            Main.getTableManager().init(tablenumber);

            return;

        }

        // if employee enters text or click cancel, close program.
        Platform.exit();
        System.exit(0);
        */

    }

    /**
     * In JavaFX the native keyboard doesn't popup when you're on a tablet.
     * Maybe we doesn't need this function in next versions, but now we have to
     * use a strange solution. ( Anyways, it works :) )
     * 
     * */
    private void showVirtualKeyboard() {
        if (textFieldZoek.isFocused()) {
            try {
                // show the virtual windows keyboard.
                Runtime.getRuntime().exec(
                        "cmd /c \"C:\\Program Files\\Common Files\\microsoft "
                                + "shared\\ink\\tabtip.exe\"");
            } catch (final Exception e) {
                LOG.log(Level.SEVERE, e.toString(), e);
            }

        }
    }

}
