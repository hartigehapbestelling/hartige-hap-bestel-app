package nl.infosys.c1.hartigehap.presentation;

import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.main.Main;
import nl.infosys.c1.hartigehap.main.Texts;

public class OrderComposer implements Initializable {

    // constants
    private static final double DURATION = 0.1;
    private static final double FT_1_FROM_X = 1;
    private static final double FT_2_FROM_X = 0.7;

    private static final int FONT_SIZE = 40;
    private static final int MIN_BUTTON_HEIGHT = 50;
    private static final int MIN_BUTTON_WIDTH = 95;
    private static final int SCALE_TRANSITION_DURATION = 600;

    @FXML
    private Button buttonDrinks;

    // custom tabs
    @FXML
    private Button buttonFirstFood;

    @FXML
    private Button buttonLastFood;

    @FXML
    private Button buttonMiddleFood;

    @FXML
    private Button buttonSelectView;

    @FXML
    private Button buttonSendOrder;
    @FXML
    private TableColumn<Item, Number> columnAantal;
    @FXML
    private TableColumn<Item, Boolean> columnBestel;
    @FXML
    private TableColumn<Item, String> columnNaam;
    @FXML
    private TableColumn<Item, String> columnOmschrijving;
    @FXML
    private TableColumn<Item, BigDecimal> columnPrijs;

    @FXML
    private TableColumn<Item, BigDecimal> columnTotaal;

    @FXML
    private TableColumn<Item, Boolean> columnVerwijder;

    // Dynamic restaurant menu
    @FXML
    private TableView<Item> menuTable;

    // show items in order = true, show menu items = false
    private boolean orderView;

    private OrderRoot root;

    @FXML
    private Label textLabelTotaalPrijs;

    /**
     * Animate the button, so user see that something is added to his future
     * order. Some kind of shopping cart.
     * */
    private void animateViewButton() {
        final ScaleTransition ft = new ScaleTransition(
                Duration.millis(SCALE_TRANSITION_DURATION), buttonSelectView);
        ft.setFromX(FT_1_FROM_X);
        ft.setToX(FT_2_FROM_X);

        ft.play();

        final ScaleTransition ft2 = new ScaleTransition(
                Duration.millis(SCALE_TRANSITION_DURATION), buttonSelectView);
        ft2.setFromX(FT_2_FROM_X);
        ft2.setToX(FT_1_FROM_X);
        ft2.play();
    }

    /**
     * Is called when user clicks the X button, the user can start over again
     * with composing an order
     * */
    @FXML
    protected void doClearOrder() {
        for (final Item item : Main.getItemManager().getItems()) {
            item.resetToStandard();
        }
        setPrice();
    }

    /**
     *
     * All handlers which are declared in FXML file (BestelGUI.fxml)
     *
     * */
    public boolean doSearch(String searchvalue) {

        if ((searchvalue != null) && !searchvalue.isEmpty()) {
            // add all searched items

            loadMenu(Main.getItemManager().searchMenuItems(searchvalue));
            return true;
        } else {

            filterFirstFood();
            // no text entered, so load full menu
            return false;
        }
    }

    /**
     * Is called when user click send order button.
     * */
    @FXML
    protected void doSendOrder() {

        // Go to OrderOverview
        root.goToTabTwo();

        // Send order to database, in responsive manner.
        new Thread(
                () -> {
                    if (Main.getOrderManager().sendOrder(Main.getItemManager(),
                            false)) {
                        succes();
                    } else {
                        error();
                    }
                }).start();

        setMenuView(true);
    }

    /**
     * Switch between composed items or the full items view (the menu)
     * 
     * */

    @FXML
    protected void doSwitchView() {
        if (orderView) {
            setOrderView();
        } else {

            setMenuView(false);
        }
        animateViewButton();
    }

    /**
     * Something went wrong with sending composed order to database, probably no
     * Internet connection.
     * */
    public void error() {
        Platform.runLater(() -> Main.getNotifier().notify(
                new Notification(Texts.COMPOSER_ERROR_TITLE_TEXT,
                        Texts.COMPOSER_ERROR_TEXT, Notification.ERROR_ICON)));
    }

    /**
     * Show only drinks is called when user clicks category Drinks.
     * */
    @FXML
    protected void filterDrinks() {
        setTransparentButtons();
        buttonDrinks.setStyle("-fx-background-color: #6F90B3");

        filterOnCategory(2);// TODO: needs recode to get from database
    }

    /**
     * Show only "voorgerechten" is called when user clicks category
     * "Voorgerechten".
     * */

    @FXML
    protected void filterFirstFood() {
        setTransparentButtons();
        buttonFirstFood.setStyle("-fx-background-color: #6F90B3");

        filterOnCategory(3);// TODO: needs recode to get from database
        // here and database.
    }

    /**
     * Show only "desserts" is called when user clicks category "Desserts".
     * */
    @FXML
    protected void filterLastFood() {
        setTransparentButtons();
        buttonLastFood.setStyle("-fx-background-color: #6F90B3");

        filterOnCategory(11);// TODO: needs recode to get from database
    }

    /**
     * Show only "hoofdgerechten" is called when user clicks category
     * "Hoofdgerechten".
     * */
    @FXML
    protected void filterMiddleFood() {
        setTransparentButtons();
        buttonMiddleFood.setStyle("-fx-background-color: #6F90B3");

        filterOnCategory(10); // TODO: needs recode to get from database
    }

    /**
     * Filter the menu on the category iD
     * 
     * */
    public boolean filterOnCategory(int category) {
        loadMenu(Main.getItemManager().searchMenuItems(category));
        return true;
    }

    /**
     * Get count of composed items
     * */
    private int getCount() {
        return Main.getItemManager().getCount();
    }

    /**
     * Initialize the composer layout
     * 
     * @param URL
     *            of the FXML file of the layout
     * @param resources
     *            map of all images and other files in the fxml file
     * */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // show menu items
        orderView = false;

        // table can't be sorted on these columns
        columnBestel.setSortable(false);
        columnVerwijder.setSortable(false);

        filterFirstFood();
    }

    /**
     * Initialize table columns
     * */
    public void initMenu() {

        // initialize table
        columnNaam.setCellValueFactory(cellData -> cellData.getValue()
                .getNameTable());
        columnOmschrijving.setCellValueFactory(cellData -> cellData.getValue()
                .getDescriptionTable());

        columnPrijs.setCellValueFactory(cellData -> cellData.getValue()
                .getPriceTable());

        columnAantal.setCellValueFactory(cellData -> cellData.getValue()
                .getCountTable());

        columnTotaal.setCellValueFactory(cellData -> cellData.getValue()
                .getTotalPriceTable());

        columnPrijs.setStyle("-fx-alignment: CENTER-RIGHT;");
        columnAantal.setStyle("-fx-alignment: CENTER;");
        columnTotaal.setStyle("-fx-alignment: CENTER-RIGHT;");
        columnNaam.getStyleClass().add("bigtext");

        // define a simple boolean cell value for the action column so that the
        // column will only be shown for non-empty rows.
        // these buttons are for compsing the order with + en - buttons
        columnVerwijder.setCellFactory(p -> new ButtonCellRemove());
        columnBestel.setCellFactory(p -> new ButtonCellAdd());

        // not overflowing text but make it responsive
        columnOmschrijving.setCellFactory(param -> {
            final TableCell<Item, String> cell = new TableCell<>();
            final Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Region.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(cell.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell;
        });
        
        menuTable.setPlaceholder(new Label("Geen items gevonden..."));

    }

    /**
     * 
     * Set items in the menu
     * */
    public void loadMenu(ObservableList<Item> observableList) {
        initMenu();
        // set observable list (real-time)
        menuTable.setItems(observableList);

        // sort in alphabetical order
        menuTable.getSortOrder().add(columnNaam);
    }

    /**
     * Show ordered items, but also change button so user can go back to all
     * items (menu, so he can choose extra items to add)
     * */
    private void setOrderView() {
        orderView = false;
        buttonSelectView.setText("MENUKAART ");

        final ObservableList<Item> orderedItems = FXCollections
                .observableArrayList();
        for (final Item item : Main.getItemManager().getItems()) {
            if (item.getCount() > 0) {
                orderedItems.add(item);
            }
        }
        loadMenu(orderedItems);
    }

    /**
     * Show full menu, but also show how many items are composed in the button
     * 
     * */
    private void setMenuView(boolean onlytext) {
        orderView = true;
        buttonSelectView.setText("HUIDIGE BESTELLING (" + getCount() + ")");

        if (!onlytext) {
            filterFirstFood();
        }

    }

    /**
     * This function sets the price in order overview, the total of all items
     * which are composed in the item manager.
     * */
    private void setPrice() {

        final DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');

        final DecimalFormat formatter = new DecimalFormat("0.00", symbols);

        textLabelTotaalPrijs.setText(formatter.format(Main.getItemManager()
                .getTotalPriceInc()));
        setMenuView(true);
        animateViewButton();
    }

    /**
     * Set the root controller, so we can change the root layout.
     * */
    public void setRootController(OrderRoot root) {
        this.root = root;
    }

    /**
     *
     * Self made tab like menu for categories in food and drinks.
     *
     * */
    public void setTransparentButtons() {
        buttonFirstFood.setStyle("-fx-background-color:transparent");
        buttonMiddleFood.setStyle("-fx-background-color:transparent");
        buttonLastFood.setStyle("-fx-background-color:transparent");
        buttonDrinks.setStyle("-fx-background-color:transparent");
    }

    /**
     * Order is send, so we enable the checkout button and we load orders in the
     * table view in the order overview
     * 
     * */
    public void succes() {
        Platform.runLater(() -> {
            root.getOrderOverview().enableButton();
            root.getOrderOverview().loadOrders();

            setPrice();
            Main.getNotifier().notify(
                    new Notification(Texts.COMPOSER_SUCCES_TITLE_TEXT,
                            Texts.COMPOSER_SUCCES_TEXT,
                            Notification.SUCCESS_ICON));
        });
    }

    /**
     * A + button is added to table view so user can change count of the items
     * 
     * */
    private class ButtonCellAdd extends TableCell<Item, Boolean> {
        final Button cellButton = new Button("+");

        ButtonCellAdd() {
            cellButton.setMinSize(MIN_BUTTON_WIDTH, MIN_BUTTON_HEIGHT);
            cellButton.setFont(Font.font(cellButton.getFont().getFamily(),
                    FONT_SIZE));
            // Action when the button is pressed
            cellButton.setOnAction(t -> {

                Main.getTableManager().userAction();
                // get Selected Item
                    final Item currentMenuItem = ButtonCellAdd.this
                            .getTableView().getItems()
                            .get(ButtonCellAdd.this.getIndex());
                    
                    //Add one more
                    currentMenuItem.changeCount(1);
                    
                  //update total price
                    setPrice();
                });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

    // Define the button cell
    private class ButtonCellRemove extends TableCell<Item, Boolean> {
        final Button cellButton = new Button("-");

        ButtonCellRemove() {
            cellButton.setMinSize(MIN_BUTTON_WIDTH, MIN_BUTTON_HEIGHT);
            cellButton.setFont(Font.font(cellButton.getFont().getFamily(),
                    FONT_SIZE));
            // Action when the button is pressed
            cellButton.setOnAction(t -> {
                // get Selected Item
                    final Item currentMenuItem = ButtonCellRemove.this
                            .getTableView().getItems()
                            .get(ButtonCellRemove.this.getIndex());
                    //Check if it won't be a negative count
                    if (currentMenuItem.getCount() != 0) {
                        
                        // - button means the count will be one less.
                        currentMenuItem.changeCount(-1);
                        
                        //update total price
                        setPrice();
                    }
                });
        }

        // Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(cellButton);
            }
        }
    }

}
