package nl.infosys.c1.hartigehap.presentation;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;
import nl.infosys.c1.hartigehap.main.Main;
import nl.infosys.c1.hartigehap.main.Texts;

public class HelpController implements Initializable {

    @FXML
    private Button closeButton;
    @FXML
    private ScrollPane scrollPane;

    @FXML
    protected void closeButtonAction() {
        // get a handle to the stage
        final Stage stage = (Stage) closeButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }

   

    @FXML
    protected void helpButtonAction() {

        // Send order to database via thread, no lag in application
        new Thread(() -> {
            if (Main.getTableManager().userWantsHelp()) {
                succes();
            } else {
                error();
            }
        }).start();

    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        //make container full screen
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

    }
    public void error() {
        Platform.runLater(() -> Main.getNotifier().notify(
                new Notification(Texts.HELP_ERROR_TITLE_TEXT,
                        Texts.HELP_ERROR_TEXT, Notification.ERROR_ICON)));
    }
    public void succes() {
        Platform.runLater(() -> Main.getNotifier().notify(
                new Notification(Texts.HELP_SUCCES_TITLE_TEXT,
                        Texts.HELP_SUCCES_TEXT, Notification.SUCCESS_ICON)));

    }
}
