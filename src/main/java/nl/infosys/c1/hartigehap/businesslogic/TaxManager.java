package nl.infosys.c1.hartigehap.businesslogic;

import nl.infosys.c1.hartigehap.datastorage.TaxDAO;

/**
 *
 * Caches the tax percentages instead of joining (mySQL) the tax table every
 * time we load items.
 *
 * */
public class TaxManager extends IntegerManager {

    public TaxManager() {
        super();
        loadIntegers(TaxDAO.getTaxes());
    }
}
