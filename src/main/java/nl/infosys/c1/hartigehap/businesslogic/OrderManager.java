package nl.infosys.c1.hartigehap.businesslogic;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import nl.infosys.c1.hartigehap.datastorage.BillDAO;
import nl.infosys.c1.hartigehap.datastorage.OrderDAO;
import nl.infosys.c1.hartigehap.datastorage.StatusDAO;
import nl.infosys.c1.hartigehap.domain.Bill;
import nl.infosys.c1.hartigehap.domain.Customer;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.domain.Order;
import nl.infosys.c1.hartigehap.main.Main;
import nl.infosys.c1.hartigehap.main.Texts;
import nl.infosys.c1.hartigehap.presentation.Notification;

public class OrderManager {
    private static final int DRINK_TYPE = 2;

    private static final int FOOD_TYPE = 1;
    private static final Logger LOG = Logger.getLogger(OrderManager.class
            .getName());
    private static final int STATUS_INTERVAL = 5000;

    private Bill bill;
    // Auto increment key only for the orders in the program.
    private final AtomicInteger count = new AtomicInteger(0);
    private final List<Order> orders;

    private StringManager statusManager;

    Timer timer = new Timer();

    /**
     * The constructor loads all cached values like the status texts. It also
     * starts a timer which keep checking the status for cached orders
     *
     * @param test
     *            - true when testing with JUnit
     *
     * */
    public OrderManager(boolean test) {
        orders = new ArrayList<Order>();

        if (test) {
            statusManager = new StringManager(StatusDAO.getTestStatusses());
        } else {
            statusManager = new StringManager(StatusDAO.getStatusses());

            // start timer
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    // every interval update orders status.
                    updateStatusses();
                }
            }, STATUS_INTERVAL, STATUS_INTERVAL);
        }
    }

    /**
     * Add orders to database or only cached list in this manager
     *
     * @param orders
     *            - List with order which needs to be added.
     * @param test
     *            - true when JUnit testing
     * */
    public boolean addOrders(List<Order> orders, boolean test) {

        if (test) {

            for (final Order order : orders) {
                final int orderId = count.incrementAndGet();
                for (final Item item : order.getItems()) {
                    item.setStatus("Verzonden");
                }
                order.setOrderID(orderId);
                order.setStatusTest(1);

                this.orders.add(order);
            }

            return true;
        }

        // auto increment id only for program because user doesn't have to see
        // that we split up orders in two
        // they just need to keep their orders sorted and descending
        final int superOrderId = count.incrementAndGet();

        for (final Order order : orders) {

            for (final Item item : order.getItems()) {
                item.setSuperOrderID(superOrderId);

                // needed to make status working in table view
                item.setStatus(order.getStatus());
            }

            try {

                if (!OrderDAO.addOrder(order, Main.getCustomerManager()
                        .getLoggedInCustomer(), Main.getTableManager()
                        .getTableNumber())) {
                    return false;
                } else {

                    setStatus(order, false, 1, false);
                    this.orders.add(order);
                }
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
                return false;
            }

        }
        return true;
    }

    public boolean checkout() {
        if (bill == null) {
            int customerID = 0;
            if (Main.getCustomerManager().getLoggedInCustomer() != null) {
                customerID = Main.getCustomerManager().getLoggedInCustomer()
                        .getCustomerID();
            }

            final Bill newbill = new Bill(getSendedOrders(), customerID);

            if (newbill != null) {
                setBill(newbill);
            }
        }

        if ((bill != null) && !bill.isSend()) {
            try {
                return BillDAO.addBill(bill, Main.getTableManager()
                        .getTableNumber());
            } catch (final SQLException e) {
                LOG.log(Level.SEVERE, e.toString(), e);
            }

        }

        return false;
    }

    public Bill getBill() {
        return bill;
    }

    public List<Order> getSendedOrders() {
        final List<Order> sendedOrders = new ArrayList<Order>();
        for (final Order order : orders) {
            if (order.isSend()) {
                sendedOrders.add(order);
            }
        }

        return sendedOrders;
    }

    public String getStatusFromId(int id) {
        return statusManager.getStringFromId(id);
    }

    public Timer getTimer() {
        return timer;
    }

    public boolean hasOrdered() {
        if (!orders.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Resets all cached orders and their items
     * */
    public void reset() {

        orders.clear();
        bill = null;

        count.set(0);

    }

    public boolean resetCustomer(boolean database) {

        // add something not database related..

        if (database) {
            OrderDAO.resetCustomer(orders);
        }

        return true;
    }

    /**
     * @return true when inserted in database
     * @param The
     *            manager of the items (added to make JUnit work)
     * @param test
     *            - true when it's a test in JUnit.
     * */
    public boolean sendOrder(ItemManager itemManager, boolean test) {

        final List<Order> orderedOrders = splitOrders(itemManager);

        if (!orderedOrders.isEmpty()) {
            if (addOrders(orderedOrders, test)) {
                return true;
            }
        } else if (hasOrdered()) {
            // if something is ordered the user can go to to the orderOverview
            return true;
        }

        // nothing is ordered or something went wrong with database connection.
        return false;
    }

    /**
     * On checkout we make a bill, but we need to reference it in the
     * orderManager to link to it later on
     *
     * @param Bill
     *            - a new bill object.
     * */
    public void setBill(Bill bill) {
        this.bill = bill;
    }

    /**
     *
     * @param Customer
     *            customer that needs to reference to the orders
     * @param
     * */
    public boolean setCustomer(Customer customer, boolean database) {

        if (!hasOrdered()) {
            return true;
        }

        if (database && !OrderDAO.setCustomer(orders, customer)) {
            // Application Thread
            Platform.runLater(() -> Main.getNotifier().notify(
                    new Notification(Texts.LOGIN_ERROR_ORDER_TITLE_TEXT,
                            Texts.LOGIN_ERROR_ORDER_TEXT,
                            Notification.ERROR_ICON)));

        }

        return true;
    }

    /**
     * This setStatus can play a sound when status is changed. Is mostly called
     * by the OrderDAO.updateStatusses()
     *
     *
     * @param Order
     *            reference
     * @param Database
     *            - Need status updated in database?
     * @param statusID
     *            - ID of the status e.g.: 1=send, 2=served etc..
     * @param alert
     *            - Does the user needs a alert of the status change?
     *
     * @return true or false: when database update isn't working.
     *
     * */
    public boolean setStatus(Order order, boolean database, int statusID,
            boolean alert) {

        order.setStatus(statusID);

        if (alert) {
            // Application Thread
            Platform.runLater(() -> {

                // Alert the user
                Main.getNotifier().notify(
                        new Notification(Texts.STATUS_TITLE_TEXT
                                + order.getStatus().toLowerCase(),
                                Texts.STATIS_TEXT, Notification.SUCCESS_ICON));

                // Play sound because customer is probably eating so doesn't
                // keep looking at his screen the whole time
                final URL resource = getClass().getResource(
                        "/notification/success.mp3");
                final Media media = new Media(resource.toString());
                final MediaPlayer mediaPlayer = new MediaPlayer(media);
                mediaPlayer.play();
            });
        }

        if (database) {
            return OrderDAO.updateStatus(order);
        }

        return true;
    }

    /**
     * Drinks and food needs to split up because they go to different
     * destination bar or restaurant. In the future we'll consider another
     * solution but we need to discuss it with the other coding teams of this
     * system.
     *
     *
     * @param Itemmanager
     *            reference to the item manager (JUnit)
     * @return Returns List with probably one or two orders in it, the contain
     *         different type of items. Drinks and food.
     *
     * */
    private List<Order> splitOrders(ItemManager itemManager) {
        // Initialize two List with different items
        final List<Item> orderedDrinks = new ArrayList<Item>();
        final List<Item> orderedFoods = new ArrayList<Item>();

        // loop the order composer items
        for (final Item item : itemManager.getItems()) {

            // if the user has clicked the + button so the count of item is
            // not above 0, continue with next iteration
            if (!(item.getCount() > 0)) {
                continue;
            }

            if (item.getType() == FOOD_TYPE) {
                // food
                final Item copyitem = new Item(item);
                orderedFoods.add(copyitem);
            } else if (item.getType() == DRINK_TYPE) {
                // drinks
                final Item copyitem = new Item(item);
                orderedDrinks.add(copyitem);
            }

            // CLEAR count on the menu
            item.resetToStandard();

        }

        // now we make two Order objects for the two different type of items
        Order orderDrinks = null;
        Order orderFoods = null;

        // check if it contains drinks
        if (!orderedDrinks.isEmpty()) {
            // Order with all ordered drinks
            orderDrinks = new Order(DRINK_TYPE, orderedDrinks);

        }

        // check if it contains items
        if (!orderedFoods.isEmpty()) {
            // Order with all ordered drinks
            orderFoods = new Order(FOOD_TYPE, orderedFoods);
        }

        // now make a list with all orders which are made
        // we make an ArrayList because in the future we may need other types
        // and we can easily adapt them with extra orders.
        final List<Order> orderedOrders = new ArrayList<Order>();

        if (orderDrinks != null) {
            orderedOrders.add(orderDrinks);
        }
        if (orderFoods != null) {
            orderedOrders.add(orderFoods);
        }
        return orderedOrders;
    }

    protected void updateStatusses() {
        OrderDAO.updateOrderStatusses(orders);

    }

}
