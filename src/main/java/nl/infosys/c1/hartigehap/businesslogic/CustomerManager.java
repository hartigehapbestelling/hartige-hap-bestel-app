package nl.infosys.c1.hartigehap.businesslogic;

import nl.infosys.c1.hartigehap.datastorage.CustomerDAO;
import nl.infosys.c1.hartigehap.domain.Customer;
import nl.infosys.c1.hartigehap.main.Main;

/**
 * This manager caches the logged in Customer
 * */
public class CustomerManager {

    private Customer loggedInCustomer;

    public CustomerManager() {

    }

    /**
     * @return cached customer
     * */
    public Customer getLoggedInCustomer() {
        return loggedInCustomer;
    }

    /**
     * @param barcode
     *            input from barcode scanner
     * @param database
     *            true when orders needs to be set in database
     * */
    public Customer login(String barcode, boolean database) {
        loggedInCustomer = CustomerDAO.getCustomer(barcode);

        Main.getOrderManager().setCustomer(loggedInCustomer, database);

        return loggedInCustomer;
    }

    /**
     * Remove cached customer
     * 
     * @param database
     *            true when orders needs reset from customer, false when it's
     *            just logout on checkout.
     * */
    public void reset(boolean database) {
        loggedInCustomer = null;
        Main.getOrderManager().resetCustomer(database);
    }
}
