package nl.infosys.c1.hartigehap.businesslogic;

import java.util.Map;

/**
 * Superclass or just a class to cache tables with a integer and a string from
 * the database. Other classes can use it to have a fast cache instead of
 * joining keys into other tables for data.
 * */
public class StringManager {
    private final Map<Integer, String> map;

    public StringManager(Map<Integer, String> map) {

        this.map = map;
    }

    public Map<Integer, String> getMap() {
        return map;
    }

    public String getStringFromId(int id) {
        return map.get(id);
    }

}
