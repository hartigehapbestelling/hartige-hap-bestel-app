package nl.infosys.c1.hartigehap.businesslogic;

import java.util.Map;

/**
 * A manager which caches two integer values, it is meant to be a cache for
 * database tables. For example the tax manager has a tax_id and a percents
 * value. Items have a tax_id instead of joining the tax table for every item to
 * get the percents we cache the tax table and get the percents value from
 * directly from the manager with the tax_id key.
 *
 * */

public class IntegerManager {
    private Map<Integer, Integer> map;

    public IntegerManager() {
    }

    public Map<Integer, Integer> getHashMap() {
        return map;
    }

    public Integer getIntFromId(int id) {
        return map.get(id);
    }

    public void loadIntegers(Map<Integer, Integer> map) {
        this.map = map;
    }
}
