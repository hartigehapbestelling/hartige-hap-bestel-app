package nl.infosys.c1.hartigehap.businesslogic;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import nl.infosys.c1.hartigehap.datastorage.CategoryDAO;
import nl.infosys.c1.hartigehap.datastorage.ImportantTextDAO;
import nl.infosys.c1.hartigehap.datastorage.ItemDAO;
import nl.infosys.c1.hartigehap.domain.Category;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.main.Main;

/**
 * itemManager - Load all items and categories from the restaurant and caches
 * them.
 *
 */
public class ItemManager {

    private final Map<Integer, Category> categories;
    private StringManager importantTextManager;
    private final ObservableList<Item> items;

    /**
     * @param test
     *            false when in production, true when JUnit tests needs to be
     *            done
     * */
    public ItemManager(boolean test) {
        items = FXCollections.observableArrayList();
        categories = new HashMap<Integer, Category>();

        if (!test) {
            loadImportantTexts();
            loadCategories();
        }

        loadItems(test);

        if (!test) {
            loadImportantTextFromItems();
        }
    }

    /**
     * @return categories which are cached
     * */
    public Map<Integer, Category> getCategories() {
        return categories;
    }

    /**
     * @param id
     *            of category
     * @return Category object which has param id.
     * */
    public Category getCategoryFromId(int id) {
        return categories.get(id);
    }

    /**
     * @param id
     *            of
     * */
    public String getImportantTextFromId(int id) {
        return importantTextManager.getStringFromId(id);
    }

    public ObservableList<Item> getItems() {
        return items;
    }
    
    /**
     * Get count of composed items
     * */
    public int getCount() {
        int count = 0;
        for (final Item item : getItems()) {
            count += item.getCount();
        }
        return count;
    }

    /**
     * @return Total price of all items in the menu which are added to order.
     * */
    public BigDecimal getTotalPriceInc() {
        BigDecimal price = new BigDecimal("0.00");
        for (final Item item : getItems()) {
            if ((item != null) && (item.getCount() > 0)) {
                price = price.add(item.getTotalPriceInc());

            }
        }
        return price;
    }

    /**
     * Loads categories from database TODO: implement better in frontend of
     * program.
     * */
    public void loadCategories() {
        final Category category = new Category(0, "Alle categorieen", 0, 1);
        categories.put(0, category);

        final Map<Integer, Category> allCategories = CategoryDAO
                .getCategories();

        for (final Category acategory : allCategories.values()) {
            if (acategory.getMainCategoryID() != 0) {
                // sub category
                acategory.setMainCategory(getCategoryFromId(acategory
                        .getMainCategoryID()));
            }

            categories.put(acategory.getCategoryID(), acategory);

        }

    }

    /**
     * Load the link table so these important text can be added to an Item
     * description.
     *
     * */
    public void loadImportantTextFromItems() {
        ImportantTextDAO.loadImportantTextsKpt(items,
                importantTextManager.getMap());
    }

    /**
     * Loads all important text like: 18+, contains soja beans etc
     *
     * */
    public void loadImportantTexts() {
        importantTextManager = new StringManager(
                ImportantTextDAO.getImportantTexts());

    }

    /**
     * Load all items from database or hard coded test data
     * 
     * @param test
     *            true when it needs har coded values for JUnit tests.
     * */
    public void loadItems(boolean test) {
        if (test) {
            items.addAll(ItemDAO.getTestItems());
        } else {
            items.addAll(ItemDAO.getItems(categories, Main.getTaxManager()
                    .getHashMap()));
        }
    }

    /**
     * Resets user composer stuff in manager like the count and the totalprice
     * on items. Needs to reset order is send or on checkout.
     *
     * */
    public void reset() {
        for (final Item item : items) {
            item.resetToStandard();
        }
    }

    /**
     *
     * filter on contains text, should definitely be recoded..
     *
     * @param CategoryID
     *            to filter on.
     * @return list with items which are being searched.
     */
    public ObservableList<Item> searchMenuItems(int categoryID) {

        final ObservableList<Item> filteredMenuItems = FXCollections
                .observableArrayList();
        // check all items in menu
        for (final Item menuItem : items) {

            if ((menuItem.getCategory().getCategoryID() == categoryID)
                    || (menuItem.getCategory().getMainCategoryID() == categoryID)) {
                // add to observable list.
                filteredMenuItems.add(menuItem);
            }
        }

        // return all items where categoryID are related to searched categoryID.
        return filteredMenuItems;
    }

    /**
     *
     * @param searchText
     *            - TextField value from search form in OrderGUI.
     * @return ObservableList - Return menu items which are being searched.
     */
    public ObservableList<Item> searchMenuItems(String searchText) {

        final ObservableList<Item> filteredMenuItems = FXCollections
                .observableArrayList();

        // check all items in menu
        for (final Item menuItem : items) {

            // If name menu-item starts with search text
            // makes sure it doesn't filter on upper case or lower case text
            if (menuItem.getName().toLowerCase()
                    .startsWith(searchText.toLowerCase())) {
                // add to observable list
                filteredMenuItems.add(menuItem);
            }
        }

        // return all items where item name contains search text
        return filteredMenuItems;
    }

}
