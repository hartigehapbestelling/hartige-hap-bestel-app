package nl.infosys.c1.hartigehap.businesslogic;

import nl.infosys.c1.hartigehap.datastorage.TableDAO;

public class TableManager {

    private static final int TABLE_ACTION = 1;
    private static final int TABLE_EMPTY = 4;
    private static final int TABLE_HELP = 3;

    private boolean firstinteraction = true;
    private int tableNumber;

    public TableManager() {
    }

    public int getTableNumber() {
        return tableNumber;
    }

    /**
     * Before user see the application, an employee set the table number in a
     * popup on startup (presentation.OrderRoot), if the result is in numbers
     * the OrderRoot calls this function.
     *
     * @param tableNumber
     *            the number of the table which the employee sets.
     */
    public void init(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    /**
     * Set table empty for next customer
     *
     * */
    public void reset() {
        new Thread(() -> {
            TableDAO.updateStatus(tableNumber, TABLE_EMPTY);

        }).start();
        firstinteraction = true;
    }

    /**
     * User interacts with the application so the bar employees need to know
     * which table is active
     *
     * */
    public void userAction() {

        if (firstinteraction) {
            firstinteraction = false;
            new Thread(() -> TableDAO.updateStatus(tableNumber, TABLE_ACTION))
            .start();
        }
    }

    /**
     * User wants help from employee, we update table status so the employees
     * know where to go.
     *
     * */
    public boolean userWantsHelp() {
        return TableDAO.updateStatus(tableNumber, TABLE_HELP);
    }
}
