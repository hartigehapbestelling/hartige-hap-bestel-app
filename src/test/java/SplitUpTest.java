import junit.framework.TestCase;
import nl.infosys.c1.hartigehap.businesslogic.ItemManager;
import nl.infosys.c1.hartigehap.businesslogic.OrderManager;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.domain.Order;

public class SplitUpTest extends TestCase {
    private ItemManager itemManager;
    private OrderManager orderManager;

    protected void setUp() throws Exception {

        itemManager = new ItemManager(true);
        orderManager = new OrderManager(true);

    }

    public void testOnlyFood() {

        // add count to items
        for (Item item : itemManager.getItems()) {
            if (item.getItemID() == 2) {
                // Antipasto
                item.changeCount(3);
            }
        }

        // Send order
        orderManager.sendOrder(itemManager, true);

        // Test if there are 1 order (food)
        assertEquals(orderManager.getSendedOrders().size(), 1);

        // Finally check if destination is right.
        for (Order order : orderManager.getSendedOrders()) {
            assertEquals(order.getDestination(), 1);
        }
    }

    public void testOnlyDrinks() {

        // add count to items
        for (Item item : itemManager.getItems()) {
            if (item.getItemID() == 3) {
                // Beer
                item.changeCount(2);
            }
        }

        // Send order
        orderManager.sendOrder(itemManager, true);

        // Test if there are 1 order (food)
        assertEquals(orderManager.getSendedOrders().size(), 1);

        // Finally check if destination is right.
        for (Order order : orderManager.getSendedOrders()) {
            // check if food
            assertEquals(order.getDestination(), 2);
        }
    }

    public void testEmptyItem() {

        // add count to items
        for (Item item : itemManager.getItems()) {
            if (item.getItemID() == 3) {
                // Beer
                item.changeCount(0);
            }

        }

        // Send order
        orderManager.sendOrder(itemManager, true);

        // Test if there is no order
        assertEquals(orderManager.getSendedOrders().size(), 0);

    }

    public void testDrinksAndOrders() {

        // add count to items
        for (Item item : itemManager.getItems()) {
            if (item.getItemID() == 3) {
                // Beer
                item.changeCount(2);
            }
            if (item.getItemID() == 2) {
                // Beer
                item.changeCount(3);
            }
        }

        // Send order
        orderManager.sendOrder(itemManager, true);

        // Test if there are 1 order (food)
        assertEquals(orderManager.getSendedOrders().size(), 2);

        int i = 0;
        // Finally check if destination is right.
        for (Order order : orderManager.getSendedOrders()) {
            // check if food
            if (i == 0) {
                assertEquals(order.getDestination(), 2);
            }
            if (i == 1) {
                assertEquals(order.getDestination(), 1);
            }
            i++;
            
        }
    }

}
