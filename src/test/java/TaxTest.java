import java.math.BigDecimal;

import nl.infosys.c1.hartigehap.businesslogic.ItemManager;
import nl.infosys.c1.hartigehap.businesslogic.OrderManager;
import nl.infosys.c1.hartigehap.domain.Bill;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.domain.Order;
import junit.framework.TestCase;

public class TaxTest extends TestCase {
    private ItemManager itemManager;
    private OrderManager orderManager;

    protected void setUp() throws Exception {

        itemManager = new ItemManager(true);
        orderManager = new OrderManager(true);

    }

    /**
     * Hand generated tested values
     * 
     * 4 Coke 1.79 exclusive 6% Tax = 4*1.79 = 7.16 3 Antipasto 7.36 tax 6% BTW
     * = 3*7.36 = 22.08 2 Beer: 1.74 tax 21% BTW = 2*1.74 = 3.48
     *
     * Total exclusive tax: 32.72
     *
     *
     * 4 Coke inclusive tax= (1,79/100)*106 * 4 = 7.5896 3 Antipasto inclusive
     * tax=(7.36/100)*106 * 3 = 23.4048 2 Beer inclusive tax= (1.74/100)*121 * 2
     * = 4.2108
     *
     * Total inclusive tax: 35.2052
     * */
    public void testTax() {

        // tax is extensively tested .
        for (Item item : itemManager.getItems()) {
            if (item.getItemID() == 1) // Coke
            {
                // 6% tax
                item.changeCount(2);// 2 Coke
                System.out.println(item.getTotalPriceEx());

                // test whether items are added
                assertEquals(item.getTotalPriceEx(), new BigDecimal("3.58"));

                item.changeCount(2);// 4 Coke
                assertEquals(item.getTotalPriceEx(), new BigDecimal("7.16"));
                assertEquals(item.getTotalPriceInc(), new BigDecimal("7.5896"));

            }
            if (item.getItemID() == 2) // antipasto
            {
                // 6% tax
                item.changeCount(3);// 3 Antipasto
                System.out.println(item.getTotalPriceEx());
                assertEquals(item.getTotalPriceEx(), new BigDecimal("22.08"));
                assertEquals(item.getTotalPriceInc(), new BigDecimal("23.4048"));

            }
            if (item.getItemID() == 3) // Beer
            {
                // 21% tax
                item.changeCount(2); // 2 Beer
                assertEquals(item.getTotalPriceEx(), new BigDecimal("3.48"));
                assertEquals(item.getTotalPriceInc(), new BigDecimal("4.2108"));

            }
        }
       
    }

   
    protected void tearDown() throws Exception {
        itemManager = null;
        orderManager = null;
    }
}