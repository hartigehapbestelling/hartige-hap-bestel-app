import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import nl.infosys.c1.hartigehap.businesslogic.ItemManager;
import nl.infosys.c1.hartigehap.businesslogic.OrderManager;
import nl.infosys.c1.hartigehap.datastorage.ItemDAO;
import nl.infosys.c1.hartigehap.domain.Bill;
import nl.infosys.c1.hartigehap.domain.Item;
import nl.infosys.c1.hartigehap.domain.Order;
import junit.framework.TestCase;

public class CheckoutTest extends TestCase {
    private ItemManager itemManager;
    private OrderManager orderManager;

    protected void setUp() throws Exception {

        itemManager = new ItemManager(true);
        orderManager = new OrderManager(true);

    }

    /**
     * Test if Bill domain is exclusive BTW is the counted the right way
     * 
     * */
    public void testBillEx() {
        
        // add count to items
        for (Item item : itemManager.getItems()) {
            if (item.getItemID() == 2) {
                // Antipasto
                item.changeCount(2);
            }
            if (item.getItemID() == 3) {
                // Beer
                item.changeCount(2);
            }
        }
        // Send order
        orderManager.sendOrder(itemManager, true);
        
        // Check tax bill
        Bill bill = new Bill(orderManager.getSendedOrders(), 0);

  
        // exclusive tax
        assertEquals(bill.getPrice(false), new BigDecimal("18.20"));

    }
    /**
     * Test if Bill domain is exclusive BTW is the counted the right way
     * 
     * */
    public void testBillInc() {
        
        // add count to items
        for (Item item : itemManager.getItems()) {
            if (item.getItemID() == 2) {
                // Antipasto
                item.changeCount(2);
            }
            if (item.getItemID() == 3) {
                // Beer
                item.changeCount(2);
            }
        }
        // Send order
        orderManager.sendOrder(itemManager, true);
        
        // Check tax bill
        Bill bill = new Bill(orderManager.getSendedOrders(), 0);

  
        System.out.println(bill.getPrice(true));
        // exclusive tax
        assertEquals(bill.getPrice(true), new BigDecimal("19.8140"));

    }
    /**
     * Test if Bill has all items added after sending
     * 
     * */
    public void testBillLessBill() {
        
        //make it the right way
        
        // add count to items
        for (Item item : itemManager.getItems()) {
            if (item.getItemID() == 2) {
                // Antipasto
                item.changeCount(2);
            }

        }
        // Send order
        orderManager.sendOrder(itemManager, true);
        Bill bill = new Bill(orderManager.getSendedOrders(), 0);


        
        List foods = new ArrayList<Item>();
        
        for(Item item : ItemDAO.getTestItems())
        {
            if (item.getItemID() == 2) {
                // Antipasto
                foods.add(item);
            }
        }
        
        Order order = new Order(1, foods);
        ArrayList<Order> orders = new ArrayList<Order>();
        orders.add(order);
        
        Bill bill2 = new Bill(orders,0);
    
        
        int billCount = 0;
        int bill2Count = 0;
        
        for(Order ordered : bill.getBilledOrders())
        {
            billCount += ordered.getItemCount();
        }
        for(Order ordered : bill2.getBilledOrders())
        {
            bill2Count += ordered.getItemCount();
        }

  

        // check if counts are really the same with the sended order function
        assertEquals(billCount, bill2Count);
        

    }


    protected void tearDown() throws Exception {
        itemManager = null;
        orderManager = null;

    }
}
